﻿using BackendApi.Services;
using FluentValidation.AspNetCore;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.BackendApi.Filters;
using KanbanDesk.Infrastructure.Persistence;
using Microsoft.AspNetCore.Mvc;
using NSwag;
using NSwag.Generation.Processors.Security;

namespace BackendApi;

public static class ConfigureServices
{
	public static IServiceCollection AddBackendApiServices(this IServiceCollection services)
	{
		services.AddDatabaseDeveloperPageExceptionFilter();

		services.AddSingleton<ICurrentUserService, CurrentUserService>();

		services.AddHttpContextAccessor();

		services.AddHealthChecks()
			.AddDbContextCheck<ApplicationDbContext>();

		services.AddControllersWithViews(options =>
			options.Filters.Add<ApiExceptionFilterAttribute>());

		services.AddFluentValidationClientsideAdapters();

		// Customise default API behaviour
		services.Configure<ApiBehaviorOptions>(options =>
			options.SuppressModelStateInvalidFilter = true);

		services.AddSwaggerDocument(configure =>
		{
			configure.Title = "KanbanDesk API";
			configure.AddSecurity("JWT", Enumerable.Empty<string>(),
				new OpenApiSecurityScheme
				{
					Type = OpenApiSecuritySchemeType.ApiKey,
					Name = "Authorization",
					In = OpenApiSecurityApiKeyLocation.Header,
					Description = "Type into the textbox: Bearer {your JWT token}."
				});

			configure.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT"));
		});

		return services;
	}
}