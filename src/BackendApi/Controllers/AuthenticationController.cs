using System.Net;
using KanbanDesk.Application.Authentication.Commands.RegisterNewUser;
using KanbanDesk.Application.Authentication.Queries;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace BackendApi.Controllers;

[ApiController]
[Route("auth/[action]")]
public class AuthenticationController : ControllerBaseWithMediator
{
	[HttpPost]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	public async Task<ActionResult> Registration(RegisterNewUserCommand command)
	{
		await Mediator.Send(command);

		return NoContent();
	}

	[HttpPost]
	[SwaggerResponse(HttpStatusCode.OK, typeof(AuthenticateWithJwtVm))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	public async Task<AuthenticateWithJwtVm> Token(AuthenticateWithJwtQuery query)
	{
		return await Mediator.Send(query);
	}
}