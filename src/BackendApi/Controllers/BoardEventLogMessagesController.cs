using System.Net;
using BackendApi.Controllers.Common;
using KanbanDesk.Application.BoardEventLogMessages.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace BackendApi.Controllers;

[Authorize]
[SwaggerResponse(HttpStatusCode.Unauthorized, typeof(void), Description = "Missing bearer authentication in header")]
public class BoardEventLogMessagesController : ApiControllerBase
{
	[HttpGet]
	[SwaggerResponse(HttpStatusCode.OK, typeof(BoardEventLogMessagesVm))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<BoardEventLogMessagesVm> Get(Guid boardId)
	{
		return await Mediator.Send(new GetBoardEventLogMessagesQuery(boardId));
	}
}