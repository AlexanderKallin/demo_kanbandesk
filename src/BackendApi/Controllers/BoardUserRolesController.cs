using System.Net;
using BackendApi.Controllers.Common;
using KanbanDesk.Application.UserBoardRoles.Commands.CreateUserBoardRole;
using KanbanDesk.Application.UserBoardRoles.Commands.DeleteUserBoardRole;
using KanbanDesk.Application.UserBoardRoles.Commands.UpdateUserBoardRole;
using KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRole;
using KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRoles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace BackendApi.Controllers;

[Authorize]
[SwaggerResponse(HttpStatusCode.Unauthorized, typeof(void), Description = "Missing bearer authentication in header")]
public class BoardUserRolesController : ApiControllerBase
{
	[HttpGet("{boardId}")]
	[ActionName("Get")]
	[SwaggerResponse(HttpStatusCode.OK, typeof(UserBoardRolesVm))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<UserBoardRolesVm> GetAll(Guid boardId)
	{
		return await Mediator.Send(new GetUserBoardRolesQuery(boardId));
	}

	[HttpGet("{userId}-{boardId}")]
	[SwaggerResponse(HttpStatusCode.OK, typeof(UserBoardRoleVm))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	public async Task<UserBoardRoleVm> Get(string userId, Guid boardId)
	{
		return await Mediator.Send(new GetUserBoardRoleQuery(userId, boardId));
	}

	[HttpPost]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	public async Task<ActionResult> Create(CreateUserBoardRoleCommand command)
	{
		await Mediator.Send(command);

		return NoContent();
	}

	[HttpPut("{userId}-{boardId}")]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	public async Task<ActionResult> Update(string userId, Guid boardId, UpdateUserBoardRoleCommand command)
	{
		if (userId != command.UserId || boardId != command.BoardId)
			return BadRequest();

		await Mediator.Send(command);

		return NoContent();
	}

	[HttpDelete("{userId}-{boardId}")]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	public async Task<ActionResult> Delete(string userId, Guid boardId)
	{
		await Mediator.Send(new DeleteUserBoardRoleCommand(userId, boardId));

		return NoContent();
	}
}