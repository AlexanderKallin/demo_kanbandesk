using System.Net;
using BackendApi.Controllers.Common;
using KanbanDesk.Application.BoardColumns.Commands.CreateBoardColumn;
using KanbanDesk.Application.BoardColumns.Commands.DeleteBoardColumn;
using KanbanDesk.Application.BoardColumns.Commands.UpdateBoardColumn;
using KanbanDesk.Application.BoardColumns.Queries.GetBoardColumn;
using KanbanDesk.Application.BoardColumns.Queries.GetBoardColumns;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace BackendApi.Controllers;

[Authorize]
[SwaggerResponse(HttpStatusCode.Unauthorized, typeof(void), Description = "Missing bearer authentication in header")]
public class BoardColumnsController : ApiControllerBase
{
	[HttpGet]
	[ActionName("Get")]
	[SwaggerResponse(HttpStatusCode.OK, typeof(BoardColumnsVm))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<BoardColumnsVm> GetAll(Guid boardId, bool includeArchived = false)
	{
		return await Mediator.Send(new GetBoardColumnsQuery(boardId, includeArchived));
	}

	[HttpGet("{id}")]
	[SwaggerResponse(HttpStatusCode.OK, typeof(BoardColumnVm))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<BoardColumnVm> Get(Guid id)
	{
		return await Mediator.Send(new GetBoardColumnQuery(id));
	}

	[HttpPost]
	[SwaggerResponse(HttpStatusCode.OK, typeof(Guid))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<Guid> Create(CreateBoardColumnCommand command)
	{
		return await Mediator.Send(command);
	}

	[HttpPut("{id}")]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<ActionResult> Update(Guid id, UpdateBoardColumnCommand command)
	{
		if (id != command.Id)
			return BadRequest();

		await Mediator.Send(command);

		return NoContent();
	}

	[HttpDelete("{id}")]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<ActionResult> Delete(Guid id)
	{
		await Mediator.Send(new DeleteBoardColumnCommand(id));

		return NoContent();
	}
}