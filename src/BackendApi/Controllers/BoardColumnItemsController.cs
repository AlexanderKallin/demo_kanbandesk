using System.Net;
using BackendApi.Controllers.Common;
using KanbanDesk.Application.BoardColumnItems.Commands.CreateBoardColumnItem;
using KanbanDesk.Application.BoardColumnItems.Commands.DeleteBoardColumnItem;
using KanbanDesk.Application.BoardColumnItems.Commands.UpdateBoardColumnItem;
using KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItem;
using KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItems;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace BackendApi.Controllers;

[Authorize]
[SwaggerResponse(HttpStatusCode.Unauthorized, typeof(void), Description = "Missing bearer authentication in header")]
public class BoardColumnItemsController : ApiControllerBase
{
	[HttpGet]
	[ActionName("Get")]
	[SwaggerResponse(HttpStatusCode.OK, typeof(BoardColumnItemsVm))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<BoardColumnItemsVm> GetAll(Guid columnId, bool includeArchived = false)
	{
		return await Mediator.Send(new GetBoardColumnItemsQuery(columnId, includeArchived));
	}

	[HttpGet("{id}")]
	[SwaggerResponse(HttpStatusCode.OK, typeof(BoardColumnItemVm))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<BoardColumnItemVm> Get(Guid id)
	{
		return await Mediator.Send(new GetBoardColumnItemQuery(id));
	}

	[HttpPost]
	[SwaggerResponse(HttpStatusCode.OK, typeof(Guid))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<Guid> Create(CreateBoardColumnItemCommand command)
	{
		return await Mediator.Send(command);
	}

	[HttpPut("{id}")]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<ActionResult> Update(Guid id, UpdateBoardColumnItemCommand command)
	{
		if (id != command.Id)
			return BadRequest();

		await Mediator.Send(command);

		return NoContent();
	}

	[HttpDelete("{id}")]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	public async Task<ActionResult> Delete(Guid id)
	{
		await Mediator.Send(new DeleteBoardColumnItemCommand(id));

		return NoContent();
	}
}