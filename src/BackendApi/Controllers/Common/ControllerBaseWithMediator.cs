using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BackendApi.Controllers;

public abstract class ControllerBaseWithMediator : ControllerBase
{
	private ISender _mediator = null!;

	protected ISender Mediator => _mediator ??= HttpContext.RequestServices.GetRequiredService<ISender>();
}