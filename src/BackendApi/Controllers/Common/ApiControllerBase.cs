﻿using Microsoft.AspNetCore.Mvc;

namespace BackendApi.Controllers.Common;

[ApiController]
[Route("api/[controller]")]
public abstract class ApiControllerBase : ControllerBaseWithMediator
{
}