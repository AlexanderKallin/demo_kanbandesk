using System.Net;
using BackendApi.Controllers.Common;
using KanbanDesk.Application.Boards.Commands.CreateBoard;
using KanbanDesk.Application.Boards.Commands.DeleteBoard;
using KanbanDesk.Application.Boards.Commands.UpdateBoard;
using KanbanDesk.Application.Boards.Queries.GetBoard;
using KanbanDesk.Application.Boards.Queries.GetBoards;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace BackendApi.Controllers;

[Authorize]
[SwaggerResponse(HttpStatusCode.Unauthorized, typeof(void), Description = "Missing bearer authentication in header")]
public class BoardsController : ApiControllerBase
{
	[HttpGet]
	[ActionName("Get")]
	[SwaggerResponse(HttpStatusCode.OK, typeof(BoardsVm))]
	public async Task<BoardsVm> GetAll()
	{
		return await Mediator.Send(new GetBoardsQuery());
	}

	[HttpGet("{id}")]
	[SwaggerResponse(HttpStatusCode.OK, typeof(BoardVm))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	public async Task<BoardVm> Get(Guid id)
	{
		return await Mediator.Send(new GetBoardQuery(id));
	}

	[HttpPost]
	[SwaggerResponse(HttpStatusCode.OK, typeof(Guid))]
	public async Task<Guid> Create(CreateBoardCommand command)
	{
		return await Mediator.Send(command);
	}

	[HttpPut("{id}")]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.BadRequest, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	public async Task<ActionResult> Update(Guid id, UpdateBoardCommand command)
	{
		if (id != command.Id)
			return BadRequest();

		await Mediator.Send(command);

		return NoContent();
	}

	[HttpDelete("{id}")]
	[SwaggerResponse(HttpStatusCode.NoContent, typeof(void))]
	[SwaggerResponse(HttpStatusCode.Forbidden, typeof(void))]
	[SwaggerResponse(HttpStatusCode.NotFound, typeof(void))]
	public async Task<ActionResult> Delete(Guid id)
	{
		await Mediator.Send(new DeleteBoardCommand(id));

		return NoContent();
	}
}