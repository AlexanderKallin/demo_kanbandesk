﻿using MediatR;

namespace KanbanDesk.Domain.Common;

public abstract class BaseEvent : INotification
{
}