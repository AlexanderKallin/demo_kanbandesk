namespace KanbanDesk.Domain.Enums;

public enum BoardRole
{
	Guest,
	Worker,
	Admin
}