namespace KanbanDesk.Domain.Entities;

public class BoardEventLogMessage
{
	public int Id { get; set; }
	public required Guid BoardId { get; set; }
	public required string InitiatorUserId { get; set; } = null!;
	public required DateTime DateTime { get; set; }
	public string? Message { get; set; }
	public Guid? SubjectId { get; set; }
}