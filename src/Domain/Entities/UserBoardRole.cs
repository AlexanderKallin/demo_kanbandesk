namespace KanbanDesk.Domain.Entities;

public class UserBoardRole
{
	public required string UserId { get; set; }
	public required Guid BoardId { get; set; }
	public required BoardRole BoardRole { get; set; }
}