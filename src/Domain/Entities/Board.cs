namespace KanbanDesk.Domain.Entities;

public class Board : BaseAuditableEntity
{
	public required string Title { get; set; } = null!;
	public IList<BoardColumn> BoardColumns { get; set; } = new List<BoardColumn>();
}