namespace KanbanDesk.Domain.Entities;

public class BoardColumn : BaseAuditableEntity
{
	public Guid BoardId { get; set; }
	public required string Title { get; set; }
	public bool IsArchived { get; set; }
	public IList<BoardColumnItem> Items { get; set; } = new List<BoardColumnItem>();
}