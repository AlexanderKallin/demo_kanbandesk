namespace KanbanDesk.Domain.Entities;

public class BoardColumnItem : BaseAuditableEntity
{
	public Guid BoardColumnId { set; get; }
	public required string Title { get; set; }
	public string? Description { get; set; }

	public bool IsArchived { get; set; }
}