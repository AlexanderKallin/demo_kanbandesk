﻿using Microsoft.AspNetCore.Identity;

namespace KanbanDesk.Domain.Entities;

public class ApplicationUser : IdentityUser
{
}