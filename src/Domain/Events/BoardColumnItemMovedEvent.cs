namespace KanbanDesk.Domain.Events;

public class BoardColumnItemMovedEvent : BaseEvent
{
	public BoardColumnItemMovedEvent(BoardColumnItem item, string initiatorUserId, Guid boardId)
	{
		Item = item;
		InitiatorUserId = initiatorUserId;
		BoardId = boardId;
	}

	public BoardColumnItem Item { get; }

	public string InitiatorUserId { get; }

	public Guid BoardId { get; }
}