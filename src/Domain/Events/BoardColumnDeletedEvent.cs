namespace KanbanDesk.Domain.Events;

public class BoardColumnDeletedEvent : BaseEvent
{
	public BoardColumnDeletedEvent(BoardColumn column)
	{
		Column = column;
	}

	public BoardColumn Column { get; }
}