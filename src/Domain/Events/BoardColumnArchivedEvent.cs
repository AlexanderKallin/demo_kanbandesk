namespace KanbanDesk.Domain.Events;

public class BoardColumnArchivedEvent : BaseEvent
{
	public BoardColumnArchivedEvent(BoardColumn column, string initiatorUserId)
	{
		Column = column;
		InitiatorUserId = initiatorUserId;
	}

	public BoardColumn Column { get; }

	public string InitiatorUserId { get; }
}