namespace KanbanDesk.Domain.Events;

public class BoardColumnItemCreatedEvent : BaseEvent
{
	public BoardColumnItemCreatedEvent(BoardColumnItem item, string userInitiatorId, Guid boardId)
	{
		Item = item;
		UserInitiatorId = userInitiatorId;
		BoardId = boardId;
	}

	public BoardColumnItem Item { get; }
	public string UserInitiatorId { get; }

	public Guid BoardId { get; }
}