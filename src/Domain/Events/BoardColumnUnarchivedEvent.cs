namespace KanbanDesk.Domain.Events;

public class BoardColumnUnarchivedEvent : BaseEvent
{
	public BoardColumnUnarchivedEvent(BoardColumn column, string initiatorUserId)
	{
		Column = column;
		InitiatorUserId = initiatorUserId;
	}

	public BoardColumn Column { get; }

	public string InitiatorUserId { get; }
}