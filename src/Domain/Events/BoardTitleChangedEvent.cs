namespace KanbanDesk.Domain.Events;

public class BoardTitleChangedEvent : BaseEvent
{
	public BoardTitleChangedEvent(Board board, string initiatorUserId, string oldTitle)
	{
		Board = board;
		InitiatorUserId = initiatorUserId;
		OldTitle = oldTitle;
	}

	public Board Board { get; }
	public string InitiatorUserId { get; }
	public string OldTitle { get; }
}