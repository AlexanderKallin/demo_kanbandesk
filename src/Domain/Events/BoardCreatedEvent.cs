namespace KanbanDesk.Domain.Events;

public class BoardCreatedEvent : BaseEvent
{
	public BoardCreatedEvent(Board board, string userInitiatorId)
	{
		Board = board;
		UserInitiatorId = userInitiatorId;
	}

	public Board Board { get; }
	public string UserInitiatorId { get; }
}