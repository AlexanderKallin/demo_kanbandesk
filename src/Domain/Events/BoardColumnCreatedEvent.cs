namespace KanbanDesk.Domain.Events;

public class BoardColumnCreatedEvent : BaseEvent
{
	public BoardColumnCreatedEvent(BoardColumn column, string userInitiatorId)
	{
		Column = column;
		UserInitiatorId = userInitiatorId;
	}

	public BoardColumn Column { get; }
	public string UserInitiatorId { get; }
}