namespace KanbanDesk.Domain.Events;

public class BoardColumnItemDeletedEvent : BaseEvent
{
	public BoardColumnItemDeletedEvent(BoardColumnItem item)
	{
		Item = item;
	}

	public BoardColumnItem Item { get; }
}