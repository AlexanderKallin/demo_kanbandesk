﻿using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Infrastructure.Identity;
using KanbanDesk.Infrastructure.Persistence;
using KanbanDesk.Infrastructure.Persistence.Interceptors;
using KanbanDesk.Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace KanbanDesk.Infrastructure;

public static class ConfigureServices
{
	public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
		IConfiguration configuration)
	{
		services.AddScoped<AuditableEntitySaveChangesInterceptor>();

		if (configuration.GetValue<bool>("UseInMemoryDatabase"))
			services.AddDbContext<ApplicationDbContext>(options =>
				options.UseInMemoryDatabase("KanbanDeskDb"));
		else
			services.AddDbContext<ApplicationDbContext>(options =>
				options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
					builder => builder.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));

		services.AddScoped<IApplicationDbContext>(provider => provider.GetRequiredService<ApplicationDbContext>());

		services.AddScoped<ApplicationDbContextInitialiser>();

		services.AddIdentity<ApplicationUser, IdentityRole>()
			.AddEntityFrameworkStores<ApplicationDbContext>()
			.AddSignInManager<SignInManager<ApplicationUser>>();

		services.AddTransient<IDateTime, DateTimeService>();
		services.AddTransient<IIdentityService, IdentityService>();
		services.AddTransient<IBoardService, BoardService>();

		var jwtConfiguration = new JwtConfiguration(
			configuration.GetValue<string>("KANBANDESK_JWT_KEY"),
			configuration.GetValue<int>("KANBANDESK_JWT_DAYSTOEXPIRE"),
			configuration.GetValue<string>("KANBANDESK_JWT_SECURITY_ALGORITHM"));

		services.AddSingleton<IJwtGeneratorService<ApplicationUser>, JwtGeneratorService>(_ =>
			new JwtGeneratorService(jwtConfiguration));

		services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
			.AddJwtBearer(opt =>
			{
				opt.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = jwtConfiguration.SymmetricSecurityKey,
					ValidateAudience = false,
					ValidateIssuer = false
				};
			});

		services.AddAuthorization(options =>
		{
			options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
				.RequireAuthenticatedUser()
				.Build();
			options.AddPolicy("CanPurge", policy => policy.RequireRole("Administrator"));
		});

		return services;
	}
}