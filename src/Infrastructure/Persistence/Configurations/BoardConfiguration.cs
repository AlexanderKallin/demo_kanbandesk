using KanbanDesk.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KanbanDesk.Infrastructure.Persistence.Configurations;

public class BoardConfiguration : IEntityTypeConfiguration<Board>
{
	public void Configure(EntityTypeBuilder<Board> builder)
	{
		builder.HasKey(board => board.Id);

		builder.Property(board => board.Title)
			.HasMaxLength(200)
			.IsRequired();

		builder
			.HasMany(board => board.BoardColumns)
			.WithOne();
	}
}