using KanbanDesk.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KanbanDesk.Infrastructure.Persistence.Configurations;

public class UserBoardRoleConfiguration : IEntityTypeConfiguration<UserBoardRole>
{
	public void Configure(EntityTypeBuilder<UserBoardRole> builder)
	{
		builder.HasKey(role => new { role.UserId, role.BoardId });

		builder
			.HasOne<Board>()
			.WithMany()
			.OnDelete(DeleteBehavior.Cascade)
			.HasForeignKey(role => role.BoardId);

		builder
			.HasOne<ApplicationUser>()
			.WithMany()
			.OnDelete(DeleteBehavior.Cascade)
			.HasForeignKey(role => role.UserId);

		builder.Property(t => t.BoardRole)
			.IsRequired();
	}
}