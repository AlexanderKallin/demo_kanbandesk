using KanbanDesk.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KanbanDesk.Infrastructure.Persistence.Configurations;

public class BoardColumnItemConfiguration : IEntityTypeConfiguration<BoardColumnItem>
{
	public void Configure(EntityTypeBuilder<BoardColumnItem> builder)
	{
		builder.HasKey(item => item.Id);

		builder
			.HasOne<BoardColumn>()
			.WithMany(column => column.Items)
			.OnDelete(DeleteBehavior.Cascade)
			.HasForeignKey(item => item.BoardColumnId);

		builder.Property(item => item.Title)
			.HasMaxLength(200)
			.IsRequired();
	}
}