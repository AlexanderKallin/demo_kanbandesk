using KanbanDesk.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KanbanDesk.Infrastructure.Persistence.Configurations;

public class BoardColumnConfiguration : IEntityTypeConfiguration<BoardColumn>
{
	public void Configure(EntityTypeBuilder<BoardColumn> builder)
	{
		builder.HasKey(column => column.Id);

		builder
			.HasOne<Board>()
			.WithMany(board => board.BoardColumns)
			.OnDelete(DeleteBehavior.Cascade)
			.HasForeignKey(column => column.BoardId);

		builder.Property(column => column.Title)
			.HasMaxLength(200)
			.IsRequired();

		builder
			.HasMany(column => column.Items)
			.WithOne();
	}
}