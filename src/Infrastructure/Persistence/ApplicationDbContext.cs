﻿using System.Reflection;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Infrastructure.Persistence.Interceptors;
using MediatR;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace KanbanDesk.Infrastructure.Persistence;

public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
{
	private readonly AuditableEntitySaveChangesInterceptor _auditableEntitySaveChangesInterceptor;
	private readonly IMediator _mediator;

	public ApplicationDbContext(
		DbContextOptions<ApplicationDbContext> options,
		IMediator mediator,
		AuditableEntitySaveChangesInterceptor auditableEntitySaveChangesInterceptor)
		: base(options)
	{
		_mediator = mediator;
		_auditableEntitySaveChangesInterceptor = auditableEntitySaveChangesInterceptor;
	}

	public DbSet<Board> Boards => Set<Board>();
	public DbSet<BoardColumn> BoardColumns => Set<BoardColumn>();
	public DbSet<BoardColumnItem> BoardColumnItems => Set<BoardColumnItem>();
	public DbSet<UserBoardRole> UserBoardRoles => Set<UserBoardRole>();
	public DbSet<BoardEventLogMessage> BoardEventLogMessages => Set<BoardEventLogMessage>();

	public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
	{
		IEnumerable<INotification> domainEvents = await _mediator.ExtractDomainEvents(this);

		var entriesWritten = await base.SaveChangesAsync(cancellationToken);

		await _mediator.PublishAll(domainEvents, cancellationToken);

		return entriesWritten;
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

		base.OnModelCreating(builder);
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		optionsBuilder.AddInterceptors(_auditableEntitySaveChangesInterceptor);
	}
}