﻿using KanbanDesk.Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace MediatR;

public static class MediatorExtensions
{
	public static async Task<IReadOnlyCollection<BaseEvent>> ExtractDomainEvents(this IMediator mediator,
		DbContext context)
	{
		var entities = context.ChangeTracker
			.Entries<BaseEntity>()
			.Where(e => e.Entity.DomainEvents.Any())
			.Select(e => e.Entity);

		var domainEvents = entities
			.SelectMany(e => e.DomainEvents)
			.ToList();

		entities.ToList().ForEach(e => e.ClearDomainEvents());

		return domainEvents;
	}

	public static async Task PublishAll(this IMediator mediator, IEnumerable<INotification> notifications,
		CancellationToken cancellationToken = default)
	{
		foreach (var notification in notifications)
			await mediator.Publish(notification, cancellationToken);
	}
}