using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace KanbanDesk.Infrastructure.Identity;

public class JwtConfiguration
{
	public readonly SigningCredentials SigningCredentials;
	public readonly SymmetricSecurityKey SymmetricSecurityKey;

	public JwtConfiguration(string? key, int? daysToExpire, string? securityAlgorithm)
	{
		if (key.IsNullOrEmpty() == false)
			Key = key!;

		if (daysToExpire is > 0)
			DaysToExpire = daysToExpire.Value;

		if (securityAlgorithm.IsNullOrEmpty() == false)
			SecurityAlgorithm = securityAlgorithm!;

		SymmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
		SigningCredentials = new SigningCredentials(SymmetricSecurityKey, SecurityAlgorithm);
	}

	public string Key { get; } = "DefaultSecurityKey1!";
	public int DaysToExpire { get; } = 7;
	public string SecurityAlgorithm { get; } = SecurityAlgorithms.HmacSha512;
}