using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using Microsoft.IdentityModel.Tokens;

namespace KanbanDesk.Infrastructure.Identity;

public class JwtGeneratorService : IJwtGeneratorService<ApplicationUser>
{
	private readonly JwtConfiguration _jwtConfiguration;

	public JwtGeneratorService(JwtConfiguration jwtConfiguration)
	{
		_jwtConfiguration = jwtConfiguration;
	}

	public string CreateToken(ApplicationUser user)
	{
		var claims = new List<Claim> { new(JwtRegisteredClaimNames.NameId, user.Id) };

		var tokenDescriptor = new SecurityTokenDescriptor
		{
			Subject = new ClaimsIdentity(claims),
			Expires = DateTime.Now.AddDays(_jwtConfiguration.DaysToExpire),
			SigningCredentials = _jwtConfiguration.SigningCredentials
		};
		var tokenHandler = new JwtSecurityTokenHandler();

		var token = tokenHandler.CreateToken(tokenDescriptor);

		return tokenHandler.WriteToken(token);
	}
}