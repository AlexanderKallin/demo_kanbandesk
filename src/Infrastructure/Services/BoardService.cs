using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Enums;

namespace KanbanDesk.Infrastructure.Services;

public class BoardService : IBoardService
{
	private readonly IApplicationDbContext _context;

	public BoardService(IApplicationDbContext context)
	{
		_context = context;
	}

	public bool IsBoardExists(Guid boardId)
	{
		return _context.Boards.Any(board => board.Id == boardId);
	}

	public bool IsUserBoardRoleExists(string userId, Guid boardId)
	{
		return _context.UserBoardRoles.Any(role =>
			role.BoardId == boardId && role.UserId == userId);
	}

	public bool IsUserBoardRoleExists(string userId, Guid boardId, BoardRole boardRole)
	{
		return _context.UserBoardRoles.Any(role =>
			role.BoardId == boardId && role.UserId == userId && role.BoardRole == boardRole);
	}

	public BoardRole? GetRole(string userId, Guid boardId)
	{
		var userBoardRole = _context.UserBoardRoles.Find(userId, boardId);
		return userBoardRole?.BoardRole;
	}
}