﻿using KanbanDesk.Application.Common.Interfaces;

namespace KanbanDesk.Infrastructure.Services;

public class DateTimeService : IDateTime
{
	public DateTime Now => DateTime.Now;
}