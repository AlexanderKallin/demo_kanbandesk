using AutoMapper;
using AutoMapper.QueryableExtensions;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRoles;

public record GetUserBoardRolesQuery(Guid BoardId) : IRequest<UserBoardRolesVm>;

public class GetUserBoardRolesQueryHandler : IRequestHandler<GetUserBoardRolesQuery, UserBoardRolesVm>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IMapper _mapper;

	public GetUserBoardRolesQueryHandler(IBoardService boardService, ICurrentUserService currentUserService,
		IApplicationDbContext context, IMapper mapper)
	{
		_boardService = boardService;
		_currentUserService = currentUserService;
		_context = context;
		_mapper = mapper;
	}

	public async Task<UserBoardRolesVm> Handle(GetUserBoardRolesQuery request, CancellationToken cancellationToken)
	{
		if (_boardService.IsBoardExists(request.BoardId) == false)
			throw new BadRequestException($"{nameof(request.BoardId)} is invalid");

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, request.BoardId) == false)
			throw new ForbiddenAccessException();

		var userBoardRolesVm = new UserBoardRolesVm
		{
			UserBoardRoles = await _context.UserBoardRoles
				.AsNoTracking()
				.Where(userBoardRole => userBoardRole.BoardId == request.BoardId)
				.ProjectTo<UserBoardRoleDto>(_mapper.ConfigurationProvider)
				.OrderByDescending(role => role.BoardRole)
				.ToListAsync(cancellationToken)
		};

		return userBoardRolesVm;
	}
}