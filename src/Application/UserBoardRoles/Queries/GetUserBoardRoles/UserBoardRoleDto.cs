using AutoMapper;
using KanbanDesk.Application.Common.Mappings;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;

namespace KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRoles;

public class UserBoardRoleDto : IMapFrom<UserBoardRole>
{
	public required string UserId { get; set; }
	public required Guid BoardId { get; set; }
	public required BoardRole BoardRole { get; set; }

	public void Mapping(Profile profile)
	{
		profile.CreateMap<UserBoardRole, UserBoardRoleDto>()
			.ForMember(destination => destination.UserId,
				options => options.MapFrom(source => source.UserId))
			.ForMember(destination => destination.BoardId,
				options => options.MapFrom(source => source.BoardId))
			.ForMember(destination => destination.BoardRole,
				options => options.MapFrom(source => source.BoardRole));
	}
}