namespace KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRoles;

public class UserBoardRolesVm
{
	public IList<UserBoardRoleDto> UserBoardRoles { get; set; } = new List<UserBoardRoleDto>();
}