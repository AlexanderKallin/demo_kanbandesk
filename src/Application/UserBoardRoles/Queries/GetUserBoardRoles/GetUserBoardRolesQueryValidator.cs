using FluentValidation;

namespace KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRoles;

public class GetUserBoardRolesQueryValidator : AbstractValidator<GetUserBoardRolesQuery>
{
	public GetUserBoardRolesQueryValidator()
	{
		RuleFor(command => command.BoardId)
			.NotEmpty()
			.WithMessage($"{nameof(GetUserBoardRolesQuery.BoardId)} is required.");
	}
}