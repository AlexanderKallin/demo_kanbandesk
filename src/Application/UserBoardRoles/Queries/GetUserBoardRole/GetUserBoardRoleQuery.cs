using AutoMapper;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using MediatR;

namespace KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRole;

public record GetUserBoardRoleQuery(string UserId, Guid BoardId) : IRequest<UserBoardRoleVm>;

public class GetUserBoardRoleQueryHandler : IRequestHandler<GetUserBoardRoleQuery, UserBoardRoleVm>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IMapper _mapper;

	public GetUserBoardRoleQueryHandler(IBoardService boardService, ICurrentUserService currentUserService,
		IApplicationDbContext context, IMapper mapper)
	{
		_boardService = boardService;
		_currentUserService = currentUserService;
		_context = context;
		_mapper = mapper;
	}

	public async Task<UserBoardRoleVm> Handle(GetUserBoardRoleQuery request, CancellationToken cancellationToken)
	{
		if (_boardService.IsBoardExists(request.BoardId) == false)
			throw new BadRequestException($"{nameof(request.BoardId)} is invalid");

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, request.BoardId) == false)
			throw new ForbiddenAccessException();

		var userBoardRole =
			await _context.UserBoardRoles.FindAsync(new object[] { request.UserId, request.BoardId },
				cancellationToken);
		if (userBoardRole == null)
			throw new NotFoundException(nameof(UserBoardRole),
				$"{nameof(UserBoardRole.UserId)}= {request.UserId} {nameof(UserBoardRole.BoardId)}= {request.BoardId}");

		return _mapper.Map<UserBoardRole, UserBoardRoleVm>(userBoardRole);
	}
}