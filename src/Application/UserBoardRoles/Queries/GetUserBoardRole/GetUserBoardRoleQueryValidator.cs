using FluentValidation;

namespace KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRole;

public class GetUserBoardRoleQueryValidator : AbstractValidator<GetUserBoardRoleQuery>
{
	public GetUserBoardRoleQueryValidator()
	{
		RuleFor(command => command.BoardId)
			.NotEmpty()
			.WithMessage($"{nameof(GetUserBoardRoleQuery.BoardId)} is required.");

		RuleFor(command => command.UserId)
			.NotEmpty()
			.WithMessage($"{nameof(GetUserBoardRoleQuery.UserId)} is required.");
	}
}