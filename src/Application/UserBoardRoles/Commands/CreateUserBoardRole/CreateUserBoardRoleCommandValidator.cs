using FluentValidation;

namespace KanbanDesk.Application.UserBoardRoles.Commands.CreateUserBoardRole;

public class CreateUserBoardRoleCommandValidator : AbstractValidator<CreateUserBoardRoleCommand>
{
	public CreateUserBoardRoleCommandValidator()
	{
		RuleFor(command => command.UserId)
			.NotEmpty()
			.WithMessage($"{nameof(CreateUserBoardRoleCommand.UserId)} is required.");

		RuleFor(command => command.BoardId)
			.NotEmpty()
			.WithMessage($"{nameof(CreateUserBoardRoleCommand.BoardId)} is required.");

		RuleFor(command => command.BoardRole)
			.IsInEnum()
			.WithMessage($"{nameof(CreateUserBoardRoleCommand.BoardRole)} should be valid.");
	}
}