using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using MediatR;

namespace KanbanDesk.Application.UserBoardRoles.Commands.CreateUserBoardRole;

public record CreateUserBoardRoleCommand(string UserId, Guid BoardId, BoardRole BoardRole) : IRequest;

public class CreateUserBoardRoleCommandHandler : IRequestHandler<CreateUserBoardRoleCommand>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IIdentityService _identityService;

	public CreateUserBoardRoleCommandHandler(IBoardService boardService, ICurrentUserService currentUserService,
		IIdentityService identityService, IApplicationDbContext context)
	{
		_boardService = boardService;
		_currentUserService = currentUserService;
		_identityService = identityService;
		_context = context;
	}

	public async Task<Unit> Handle(CreateUserBoardRoleCommand request, CancellationToken cancellationToken)
	{
		if (_boardService.IsBoardExists(request.BoardId) == false)
			throw new BadRequestException($"{nameof(request.BoardId)} is invalid");

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, request.BoardId, BoardRole.Admin) == false)
			throw new ForbiddenAccessException();

		if (_identityService.IsUserExists(request.UserId) == false)
			throw new BadRequestException($"{nameof(ApplicationUser)} Id= {request.UserId} not found");

		if (_boardService.IsUserBoardRoleExists(request.UserId, request.BoardId))
			throw new BadRequestException("User already has role in this board.");

		var userBoardRole = new UserBoardRole
		{
			UserId = request.UserId,
			BoardId = request.BoardId,
			BoardRole = request.BoardRole
		};

		await _context.UserBoardRoles.AddAsync(userBoardRole, cancellationToken);
		await _context.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}