using FluentValidation;

namespace KanbanDesk.Application.UserBoardRoles.Commands.DeleteUserBoardRole;

public class DeleteUserBoardRoleCommandValidator : AbstractValidator<DeleteUserBoardRoleCommand>
{
	public DeleteUserBoardRoleCommandValidator()
	{
		RuleFor(command => command.UserId)
			.NotEmpty()
			.WithMessage($"{nameof(DeleteUserBoardRoleCommand.UserId)} is required.");

		RuleFor(command => command.BoardId)
			.NotEmpty()
			.WithMessage($"{nameof(DeleteUserBoardRoleCommand.BoardId)} is required.");
	}
}