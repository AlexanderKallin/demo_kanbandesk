using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using MediatR;

namespace KanbanDesk.Application.UserBoardRoles.Commands.DeleteUserBoardRole;

public record DeleteUserBoardRoleCommand(string UserId, Guid BoardId) : IRequest;

public class DeleteUserBoardRoleCommandHandler : IRequestHandler<DeleteUserBoardRoleCommand>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public DeleteUserBoardRoleCommandHandler(IBoardService boardService, ICurrentUserService currentUserService,
		IApplicationDbContext context)
	{
		_boardService = boardService;
		_currentUserService = currentUserService;
		_context = context;
	}

	public async Task<Unit> Handle(DeleteUserBoardRoleCommand request, CancellationToken cancellationToken)
	{
		var board = await _context.Boards.FindAsync(new object[] { request.BoardId }, cancellationToken);
		if (board == null)
			throw new BadRequestException($"{nameof(request.BoardId)} is invalid");

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, request.BoardId, BoardRole.Admin) == false)
			throw new ForbiddenAccessException();

		var userBoardRole =
			await _context.UserBoardRoles.FindAsync(new object[] { request.UserId, request.BoardId },
				cancellationToken);
		if (userBoardRole == null)
			throw new NotFoundException(nameof(UserBoardRole),
				$"{nameof(UserBoardRole.UserId)}= {request.UserId} {nameof(UserBoardRole.BoardId)}= {request.BoardId}");

		if (request.UserId == board.CreatedBy)
			throw new BadRequestException("Unable to delete board creator's role");

		_context.UserBoardRoles.Remove(userBoardRole);
		await _context.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}