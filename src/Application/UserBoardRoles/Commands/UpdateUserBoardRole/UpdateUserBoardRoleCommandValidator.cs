using FluentValidation;

namespace KanbanDesk.Application.UserBoardRoles.Commands.UpdateUserBoardRole;

public class UpdateUserBoardRoleCommandValidator : AbstractValidator<UpdateUserBoardRoleCommand>
{
	public UpdateUserBoardRoleCommandValidator()
	{
		RuleFor(command => command.UserId)
			.NotEmpty()
			.WithMessage($"{nameof(UpdateUserBoardRoleCommand.UserId)} is required.");

		RuleFor(command => command.BoardId)
			.NotEmpty()
			.WithMessage($"{nameof(UpdateUserBoardRoleCommand.BoardId)} is required.");

		RuleFor(command => command.BoardRole)
			.IsInEnum()
			.WithMessage($"{nameof(UpdateUserBoardRoleCommand.BoardRole)} should be valid.");
	}
}