using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using MediatR;

namespace KanbanDesk.Application.UserBoardRoles.Commands.UpdateUserBoardRole;

public record UpdateUserBoardRoleCommand(string UserId, Guid BoardId, BoardRole BoardRole) : IRequest;

public class UpdateUserBoardRoleCommandHandler : IRequestHandler<UpdateUserBoardRoleCommand>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public UpdateUserBoardRoleCommandHandler(ICurrentUserService currentUserService, IBoardService boardService,
		IApplicationDbContext context)
	{
		_currentUserService = currentUserService;
		_boardService = boardService;
		_context = context;
	}

	public async Task<Unit> Handle(UpdateUserBoardRoleCommand request, CancellationToken cancellationToken)
	{
		var initiatorUserId = _currentUserService.UserId!;

		var board = await _context.Boards.FindAsync(new object[] { request.BoardId }, cancellationToken);
		if (board == null)
			throw new BadRequestException($"{nameof(request.BoardId)} is invalid");

		if (_boardService.IsUserBoardRoleExists(initiatorUserId, request.BoardId, BoardRole.Admin) == false)
			throw new ForbiddenAccessException();

		if (request.UserId == initiatorUserId)
			throw new BadRequestException("User can't change their own role");

		var userBoardRole =
			await _context.UserBoardRoles.FindAsync(new object[] { request.UserId, request.BoardId },
				cancellationToken);
		if (userBoardRole == null)
			throw new NotFoundException(nameof(UserBoardRole),
				$"{nameof(UserBoardRole.UserId)}= {request.UserId} {nameof(UserBoardRole.BoardId)}= {request.BoardId}");

		if (request.UserId == board.CreatedBy)
			throw new BadRequestException("Unable to change board creator's role");

		if (userBoardRole.BoardRole == request.BoardRole)
			throw new BadRequestException("User already in this role.");

		userBoardRole.BoardRole = request.BoardRole;
		await _context.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}