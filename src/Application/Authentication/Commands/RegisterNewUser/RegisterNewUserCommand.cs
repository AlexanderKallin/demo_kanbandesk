using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using MediatR;

namespace KanbanDesk.Application.Authentication.Commands.RegisterNewUser;

public class RegisterNewUserCommand : IRequest
{
	public required string Email { get; init; }
	public required string Password { get; init; }

	public override string ToString()
	{
		return $"{GetType().Name} {{ {nameof(Email)} = {Email} }}";
	}
}

public class RegisterNewUserCommandHandler : IRequestHandler<RegisterNewUserCommand>
{
	private readonly IIdentityService _identityService;

	public RegisterNewUserCommandHandler(IIdentityService identityService)
	{
		_identityService = identityService;
	}
	
	public async Task<Unit> Handle(RegisterNewUserCommand request, CancellationToken cancellationToken)
	{
		var result = await _identityService.CreateUserAsync(request.Email, request.Password);
		if (result.Result.Succeeded == false)
			throw new BadRequestException("Invalid credentials");

		return Unit.Value;
	}
}