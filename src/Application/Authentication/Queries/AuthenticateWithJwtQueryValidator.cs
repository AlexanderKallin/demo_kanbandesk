using FluentValidation;

namespace KanbanDesk.Application.Authentication.Queries;

public class AuthenticateWithJwtQueryValidator : AbstractValidator<AuthenticateWithJwtQuery>
{
	public AuthenticateWithJwtQueryValidator()
	{
		RuleFor(query => query.Email)
			.NotEmpty()
			.WithMessage("Email is required.");
		RuleFor(query => query.Password)
			.NotEmpty()
			.WithMessage("Password is required.");
	}
}