namespace KanbanDesk.Application.Authentication.Queries;

public class AuthenticateWithJwtVm
{
	public string TokenType { get; set; }
	public string Token { get; set; }
}