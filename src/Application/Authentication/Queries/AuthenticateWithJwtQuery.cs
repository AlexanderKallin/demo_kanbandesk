using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace KanbanDesk.Application.Authentication.Queries;

public class AuthenticateWithJwtQuery : IRequest<AuthenticateWithJwtVm>
{
	public required string Email { get; init; }
	public required string Password { get; init; }

	public override string ToString()
	{
		return $"{GetType().Name} {{ {nameof(Email)} = {Email} }}";
	}
}

public class AuthenticateWithJwtQueryHandler : IRequestHandler<AuthenticateWithJwtQuery, AuthenticateWithJwtVm>
{
	private readonly IJwtGeneratorService<ApplicationUser> _jwtGeneratorService;
	private readonly SignInManager<ApplicationUser> _signInManager;
	private readonly UserManager<ApplicationUser> _userManager;

	public AuthenticateWithJwtQueryHandler(
		UserManager<ApplicationUser> userManager,
		SignInManager<ApplicationUser> signInManager,
		IJwtGeneratorService<ApplicationUser> jwtGeneratorService
	)
	{
		_userManager = userManager;
		_signInManager = signInManager;
		_jwtGeneratorService = jwtGeneratorService;
	}

	public async Task<AuthenticateWithJwtVm> Handle(AuthenticateWithJwtQuery request,
		CancellationToken cancellationToken)
	{
		var user = await _userManager.FindByEmailAsync(request.Email);

		if (user == null)
			throw new AuthenticationFailedException();

		var signInResult = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);

		if (signInResult.Succeeded == false)
			throw new AuthenticationFailedException();

		return new AuthenticateWithJwtVm
		{
			TokenType = "Bearer",
			Token = _jwtGeneratorService.CreateToken(user)
		};
	}
}