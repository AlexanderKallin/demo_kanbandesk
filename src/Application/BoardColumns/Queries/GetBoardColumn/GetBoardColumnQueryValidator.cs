using FluentValidation;

namespace KanbanDesk.Application.BoardColumns.Queries.GetBoardColumn;

public class GetBoardColumnQueryValidator : AbstractValidator<GetBoardColumnQuery>
{
	public GetBoardColumnQueryValidator()
	{
		RuleFor(t => t.Id)
			.NotEmpty()
			.WithMessage($"{nameof(GetBoardColumnQuery.Id)} is required.");
	}
}