using AutoMapper;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace KanbanDesk.Application.BoardColumns.Queries.GetBoardColumn;

public record GetBoardColumnQuery(Guid Id) : IRequest<BoardColumnVm>;

public class GetBoardColumnQueryHandler : IRequestHandler<GetBoardColumnQuery, BoardColumnVm>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IMapper _mapper;

	public GetBoardColumnQueryHandler(IApplicationDbContext context, ICurrentUserService currentUserService,
		IBoardService boardService, IMapper mapper)
	{
		_context = context;
		_currentUserService = currentUserService;
		_boardService = boardService;
		_mapper = mapper;
	}

	public async Task<BoardColumnVm> Handle(GetBoardColumnQuery request, CancellationToken cancellationToken)
	{
		var column = await _context.BoardColumns
			.Include(c => c.Items.Where(item => item.IsArchived == false))
			.FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

		if (column == null)
			throw new NotFoundException(nameof(BoardColumn), request.Id);

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, column.BoardId) == false)
			throw new ForbiddenAccessException();

		return _mapper.Map<BoardColumn, BoardColumnVm>(column);
	}
}