using System.Linq.Expressions;
using AutoMapper;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace KanbanDesk.Application.BoardColumns.Queries.GetBoardColumns;

public record GetBoardColumnsQuery(Guid BoardId, bool IncludeArchived) : IRequest<BoardColumnsVm>;

public class GetBoardColumnsQueryHandler : IRequestHandler<GetBoardColumnsQuery, BoardColumnsVm>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IMapper _mapper;

	public GetBoardColumnsQueryHandler(IApplicationDbContext context, IBoardService boardService,
		ICurrentUserService currentUserService, IMapper mapper)
	{
		_context = context;
		_boardService = boardService;
		_currentUserService = currentUserService;
		_mapper = mapper;
	}

	public async Task<BoardColumnsVm> Handle(GetBoardColumnsQuery request, CancellationToken cancellationToken)
	{
		if (_boardService.IsBoardExists(request.BoardId) == false)
			throw new BadRequestException($"{nameof(request.BoardId)} is invalid");

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, request.BoardId) == false)
			throw new ForbiddenAccessException();

		Expression<Func<BoardColumn, bool>> expression = column => column.BoardId == request.BoardId;

		if (request.IncludeArchived == false)
			expression = column => column.BoardId == request.BoardId && column.IsArchived == false;

		var columns = await _context.BoardColumns
			.AsNoTracking()
			.Include(column => column.Items.Where(item => item.IsArchived == false))
			.AsSplitQuery()
			.Where(expression)
			.ToListAsync(cancellationToken);

		return new BoardColumnsVm
		{
			BoardColumns = _mapper.Map<List<BoardColumn>, List<BoardColumnDto>>(columns)
		};
	}
}