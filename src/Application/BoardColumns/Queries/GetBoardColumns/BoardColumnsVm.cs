namespace KanbanDesk.Application.BoardColumns.Queries.GetBoardColumns;

public class BoardColumnsVm
{
	public required IList<BoardColumnDto> BoardColumns { get; set; } = new List<BoardColumnDto>();
}