using FluentValidation;

namespace KanbanDesk.Application.BoardColumns.Queries.GetBoardColumns;

public class GetBoardColumnsQueryValidator : AbstractValidator<GetBoardColumnsQuery>
{
	public GetBoardColumnsQueryValidator()
	{
		RuleFor(command => command.BoardId)
			.NotEmpty()
			.WithMessage($"{nameof(GetBoardColumnsQuery.BoardId)} is required.");
	}
}