using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumns.Commands.DeleteBoardColumn;

public record DeleteBoardColumnCommand(Guid Id) : IRequest;

public class DeleteBoardColumnCommandHandler : IRequestHandler<DeleteBoardColumnCommand>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public DeleteBoardColumnCommandHandler(IApplicationDbContext context, IBoardService boardService,
		ICurrentUserService currentUserService)
	{
		_context = context;
		_boardService = boardService;
		_currentUserService = currentUserService;
	}

	public async Task<Unit> Handle(DeleteBoardColumnCommand request, CancellationToken cancellationToken)
	{
		var column = await _context.BoardColumns.FindAsync(new object[] { request.Id }, cancellationToken);
		if (column == null)
			throw new NotFoundException(nameof(BoardColumn), request.Id);

		var initiatorUserRole = _boardService.GetRole(_currentUserService.UserId!, column.BoardId);
		if (initiatorUserRole == null ||
		    (initiatorUserRole != BoardRole.Admin && initiatorUserRole != BoardRole.Worker))
			throw new ForbiddenAccessException();

		column.AddDomainEvent(new BoardColumnDeletedEvent(column));

		_context.BoardColumns.Remove(column);
		await _context.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}