using FluentValidation;

namespace KanbanDesk.Application.BoardColumns.Commands.DeleteBoardColumn;

public class DeleteBoardColumnCommandValidator : AbstractValidator<DeleteBoardColumnCommand>
{
	public DeleteBoardColumnCommandValidator()
	{
		RuleFor(command => command.Id)
			.NotEmpty()
			.WithMessage($"{nameof(DeleteBoardColumnCommand.Id)} is required.");
	}
}