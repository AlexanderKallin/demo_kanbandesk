using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumns.Commands.CreateBoardColumn;

public record CreateBoardColumnCommand(Guid BoardId, string Title) : IRequest<Guid>;

public class CreateBoardColumnCommandHandler : IRequestHandler<CreateBoardColumnCommand, Guid>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public CreateBoardColumnCommandHandler(ICurrentUserService currentUserService, IBoardService boardService,
		IApplicationDbContext context)
	{
		_currentUserService = currentUserService;
		_boardService = boardService;
		_context = context;
	}

	public async Task<Guid> Handle(CreateBoardColumnCommand request, CancellationToken cancellationToken)
	{
		var initiatorUserId = _currentUserService.UserId!;

		if (_boardService.IsBoardExists(request.BoardId) == false)
			throw new BadRequestException($"{nameof(request.BoardId)} is invalid");

		var initiatorUserRole = _boardService.GetRole(initiatorUserId, request.BoardId);
		if (initiatorUserRole == null ||
		    (initiatorUserRole != BoardRole.Admin && initiatorUserRole != BoardRole.Worker))
			throw new ForbiddenAccessException();

		var column = new BoardColumn
		{
			BoardId = request.BoardId,
			Title = request.Title
		};

		column.AddDomainEvent(new BoardColumnCreatedEvent(column, initiatorUserId));

		await _context.BoardColumns.AddAsync(column, cancellationToken);
		await _context.SaveChangesAsync(cancellationToken);

		return column.Id;
	}
}