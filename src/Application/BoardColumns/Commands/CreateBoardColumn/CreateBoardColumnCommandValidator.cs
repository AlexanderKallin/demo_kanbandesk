using FluentValidation;

namespace KanbanDesk.Application.BoardColumns.Commands.CreateBoardColumn;

public class CreateBoardColumnCommandValidator : AbstractValidator<CreateBoardColumnCommand>
{
	public CreateBoardColumnCommandValidator()
	{
		RuleFor(command => command.BoardId)
			.NotEmpty()
			.WithMessage($"{nameof(CreateBoardColumnCommand.BoardId)} is required.");

		RuleFor(command => command.Title)
			.NotEmpty()
			.WithMessage($"{nameof(CreateBoardColumnCommand.Title)} is required.")
			.MaximumLength(200)
			.WithMessage("Title must not exceed 200 characters.");
	}
}