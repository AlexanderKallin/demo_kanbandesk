using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumns.Commands.UpdateBoardColumn;

public record UpdateBoardColumnCommand(Guid Id, string Title, bool IsArchived) : IRequest;

public class UpdateBoardColumnCommandHandler : IRequestHandler<UpdateBoardColumnCommand>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public UpdateBoardColumnCommandHandler(IApplicationDbContext context, ICurrentUserService currentUserService,
		IBoardService boardService)
	{
		_context = context;
		_currentUserService = currentUserService;
		_boardService = boardService;
	}

	public async Task<Unit> Handle(UpdateBoardColumnCommand request, CancellationToken cancellationToken)
	{
		var column = await _context.BoardColumns.FindAsync(new object[] { request.Id }, cancellationToken);
		if (column == null)
			throw new NotFoundException(nameof(BoardColumn), request.Id);

		var initiatorUserId = _currentUserService.UserId!;

		var initiatorUserRole = _boardService.GetRole(initiatorUserId, column.BoardId);
		if (initiatorUserRole == null ||
		    (initiatorUserRole != BoardRole.Admin && initiatorUserRole != BoardRole.Worker))
			throw new ForbiddenAccessException();

		column.Title = request.Title;

		if (column.IsArchived == false && request.IsArchived)
			column.AddDomainEvent(new BoardColumnArchivedEvent(column, initiatorUserId));

		if (column.IsArchived && request.IsArchived == false)
			column.AddDomainEvent(new BoardColumnUnarchivedEvent(column, initiatorUserId));

		column.IsArchived = request.IsArchived;

		await _context.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}