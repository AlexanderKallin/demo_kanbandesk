using FluentValidation;

namespace KanbanDesk.Application.BoardColumns.Commands.UpdateBoardColumn;

public class UpdateBoardColumnCommandValidator : AbstractValidator<UpdateBoardColumnCommand>
{
	public UpdateBoardColumnCommandValidator()
	{
		RuleFor(command => command.Id)
			.NotEmpty()
			.WithMessage($"{nameof(UpdateBoardColumnCommand.Id)} is required.");

		RuleFor(command => command.Title)
			.NotEmpty()
			.WithMessage($"{nameof(UpdateBoardColumnCommand.Title)} is required.")
			.MaximumLength(200)
			.WithMessage("Title must not exceed 200 characters.");
	}
}