using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumns.EventHandlers;

public class BoardColumnCreatedEventHandler : INotificationHandler<BoardColumnCreatedEvent>
{
	private readonly IApplicationDbContext _context;

	public BoardColumnCreatedEventHandler(IApplicationDbContext context)
	{
		_context = context;
	}

	public async Task Handle(BoardColumnCreatedEvent notification, CancellationToken cancellationToken)
	{
		var eventLogMessage = new BoardEventLogMessage
		{
			BoardId = notification.Column.BoardId,
			InitiatorUserId = notification.UserInitiatorId,
			DateTime = DateTime.Now,
			Message = "created new column",
			SubjectId = notification.Column.Id
		};

		await _context.BoardEventLogMessages.AddAsync(eventLogMessage, cancellationToken);

		await _context.SaveChangesAsync(cancellationToken);
	}
}