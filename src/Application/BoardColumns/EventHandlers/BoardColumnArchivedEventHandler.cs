using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumns.EventHandlers;

public class BoardColumnArchivedEventHandler : INotificationHandler<BoardColumnArchivedEvent>
{
	private readonly IApplicationDbContext _context;

	public BoardColumnArchivedEventHandler(IApplicationDbContext context)
	{
		_context = context;
	}

	public async Task Handle(BoardColumnArchivedEvent notification, CancellationToken cancellationToken)
	{
		var eventLogMessage = new BoardEventLogMessage
		{
			BoardId = notification.Column.BoardId,
			InitiatorUserId = notification.InitiatorUserId,
			DateTime = DateTime.Now,
			Message = "archived column",
			SubjectId = notification.Column.Id
		};

		await _context.BoardEventLogMessages.AddAsync(eventLogMessage, cancellationToken);

		await _context.SaveChangesAsync(cancellationToken);
	}
}