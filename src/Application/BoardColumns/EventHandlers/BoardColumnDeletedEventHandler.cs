using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumns.EventHandlers;

public class BoardColumnDeletedEventHandler : INotificationHandler<BoardColumnDeletedEvent>
{
	private readonly IApplicationDbContext _context;

	public BoardColumnDeletedEventHandler(IApplicationDbContext context)
	{
		_context = context;
	}

	public async Task Handle(BoardColumnDeletedEvent notification, CancellationToken cancellationToken)
	{
		var associatedEventLogMessages =
			_context.BoardEventLogMessages.Where(eventLogMessage =>
				eventLogMessage.SubjectId == notification.Column.Id);
		_context.BoardEventLogMessages.RemoveRange(associatedEventLogMessages);

		await _context.SaveChangesAsync(cancellationToken);
	}
}