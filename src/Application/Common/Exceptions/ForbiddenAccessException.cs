﻿namespace KanbanDesk.Application.Common.Exceptions;

public class ForbiddenAccessException : Exception
{
}