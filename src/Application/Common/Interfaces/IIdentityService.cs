﻿using KanbanDesk.Application.Common.Models;
using KanbanDesk.Domain.Entities;

namespace KanbanDesk.Application.Common.Interfaces;

public interface IIdentityService
{
	Task<string?> GetUserNameAsync(string userId);
	Task<ApplicationUser> GetUserByIdAsync(string userId);

	bool IsUserExists(string userId);

	Task<bool> IsInRoleAsync(string userId, string role);

	Task<bool> AuthorizeAsync(string userId, string policyName);

	Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password);

	Task<Result> DeleteUserAsync(string userId);
}