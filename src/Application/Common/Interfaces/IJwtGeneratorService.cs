namespace KanbanDesk.Application.Common.Interfaces;

public interface IJwtGeneratorService<T>
{
	string CreateToken(T user);
}