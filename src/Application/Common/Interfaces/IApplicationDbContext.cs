﻿using KanbanDesk.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace KanbanDesk.Application.Common.Interfaces;

public interface IApplicationDbContext
{
	DbSet<Board> Boards { get; }
	DbSet<BoardColumn> BoardColumns { get; }
	DbSet<BoardColumnItem> BoardColumnItems { get; }
	DbSet<UserBoardRole> UserBoardRoles { get; }
	DbSet<BoardEventLogMessage> BoardEventLogMessages { get; }
	Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}