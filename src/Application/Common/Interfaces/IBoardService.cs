using KanbanDesk.Domain.Enums;

namespace KanbanDesk.Application.Common.Interfaces;

public interface IBoardService
{
	bool IsBoardExists(Guid boardId);
	bool IsUserBoardRoleExists(string userId, Guid boardId);
	bool IsUserBoardRoleExists(string userId, Guid boardId, BoardRole boardRole);
	BoardRole? GetRole(string userId, Guid boardId);
}