using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumnItems.EventHandlers;

public class BoardColumnItemMovedEventHandler : INotificationHandler<BoardColumnItemMovedEvent>
{
	private readonly IApplicationDbContext _context;

	public BoardColumnItemMovedEventHandler(IApplicationDbContext context)
	{
		_context = context;
	}

	public async Task Handle(BoardColumnItemMovedEvent notification, CancellationToken cancellationToken)
	{
		var eventLogMessage = new BoardEventLogMessage
		{
			BoardId = notification.BoardId,
			InitiatorUserId = notification.InitiatorUserId,
			DateTime = DateTime.Now,
			Message = $"moved item to {notification.Item.BoardColumnId}",
			SubjectId = notification.Item.Id
		};

		await _context.BoardEventLogMessages.AddAsync(eventLogMessage, cancellationToken);

		await _context.SaveChangesAsync(cancellationToken);
	}
}