using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Events;

namespace KanbanDesk.Application.BoardColumnItems.EventHandlers;

public class BoardColumnItemUnarchivedEventHandler
{
	private readonly IApplicationDbContext _context;

	public BoardColumnItemUnarchivedEventHandler(IApplicationDbContext context)
	{
		_context = context;
	}

	public async Task Handle(BoardColumnItemArchivedEvent notification, CancellationToken cancellationToken)
	{
		var eventLogMessage = new BoardEventLogMessage
		{
			BoardId = notification.BoardId,
			InitiatorUserId = notification.InitiatorUserId,
			DateTime = DateTime.Now,
			Message = "unarchived item",
			SubjectId = notification.Item.Id
		};

		await _context.BoardEventLogMessages.AddAsync(eventLogMessage, cancellationToken);

		await _context.SaveChangesAsync(cancellationToken);
	}
}