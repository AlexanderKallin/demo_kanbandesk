using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumnItems.EventHandlers;

public class BoardColumnItemCreatedEventHandler : INotificationHandler<BoardColumnItemCreatedEvent>
{
	private readonly IApplicationDbContext _context;

	public BoardColumnItemCreatedEventHandler(IApplicationDbContext context)
	{
		_context = context;
	}

	public async Task Handle(BoardColumnItemCreatedEvent notification, CancellationToken cancellationToken)
	{
		var eventLogMessage = new BoardEventLogMessage
		{
			BoardId = notification.BoardId,
			InitiatorUserId = notification.UserInitiatorId,
			DateTime = DateTime.Now,
			Message = "created new item",
			SubjectId = notification.Item.Id
		};

		await _context.BoardEventLogMessages.AddAsync(eventLogMessage, cancellationToken);

		await _context.SaveChangesAsync(cancellationToken);
	}
}