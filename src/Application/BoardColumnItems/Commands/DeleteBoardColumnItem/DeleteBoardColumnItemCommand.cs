using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumnItems.Commands.DeleteBoardColumnItem;

public record DeleteBoardColumnItemCommand(Guid Id) : IRequest;

public class DeleteBoardColumnItemCommandHandler : IRequestHandler<DeleteBoardColumnItemCommand>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public DeleteBoardColumnItemCommandHandler(IApplicationDbContext context, ICurrentUserService currentUserService,
		IBoardService boardService)
	{
		_context = context;
		_currentUserService = currentUserService;
		_boardService = boardService;
	}

	public async Task<Unit> Handle(DeleteBoardColumnItemCommand request, CancellationToken cancellationToken)
	{
		var initiatorUserId = _currentUserService.UserId!;

		var item = await _context.BoardColumnItems.FindAsync(new object?[] { request.Id }, cancellationToken);
		if (item == null)
			throw new NotFoundException(nameof(BoardColumnItem), request.Id);

		var column = _context.BoardColumns.FirstOrDefault(column => column.Items.Contains(item));

		var initiatorUserRole = _boardService.GetRole(initiatorUserId, column!.BoardId);
		if (initiatorUserRole == null ||
		    (initiatorUserRole != BoardRole.Admin && initiatorUserRole != BoardRole.Worker))
			throw new ForbiddenAccessException();

		item.AddDomainEvent(new BoardColumnItemDeletedEvent(item));

		_context.BoardColumnItems.Remove(item);
		await _context.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}