using FluentValidation;

namespace KanbanDesk.Application.BoardColumnItems.Commands.DeleteBoardColumnItem;

public class DeleteBoardColumnItemCommandValidator : AbstractValidator<DeleteBoardColumnItemCommand>
{
	public DeleteBoardColumnItemCommandValidator()
	{
		RuleFor(command => command.Id)
			.NotEmpty()
			.WithMessage($"{nameof(DeleteBoardColumnItemCommand.Id)} is required.");
	}
}