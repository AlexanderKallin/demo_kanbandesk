using FluentValidation;

namespace KanbanDesk.Application.BoardColumnItems.Commands.CreateBoardColumnItem;

public class CreateBoardColumnItemCommandValidator : AbstractValidator<CreateBoardColumnItemCommand>
{
	public CreateBoardColumnItemCommandValidator()
	{
		RuleFor(command => command.BoardColumnId)
			.NotEmpty()
			.WithMessage($"{nameof(CreateBoardColumnItemCommand.BoardColumnId)} is required.");

		RuleFor(command => command.Title)
			.NotEmpty()
			.WithMessage($"{nameof(CreateBoardColumnItemCommand.Title)} is required.")
			.MaximumLength(200)
			.WithMessage("Title must not exceed 200 characters.");
	}
}