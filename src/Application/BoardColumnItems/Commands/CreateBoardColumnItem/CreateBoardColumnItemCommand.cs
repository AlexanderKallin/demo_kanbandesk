using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumnItems.Commands.CreateBoardColumnItem;

public record CreateBoardColumnItemCommand(Guid BoardColumnId, string Title, string Description) : IRequest<Guid>;

public class CreateCardCommandHandler : IRequestHandler<CreateBoardColumnItemCommand, Guid>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public CreateCardCommandHandler(ICurrentUserService currentUserService, IApplicationDbContext context,
		IBoardService boardService)
	{
		_currentUserService = currentUserService;
		_context = context;
		_boardService = boardService;
	}

	public async Task<Guid> Handle(CreateBoardColumnItemCommand request, CancellationToken cancellationToken)
	{
		var initiatorUserId = _currentUserService.UserId!;

		var column = await _context.BoardColumns.FindAsync(new object?[] { request.BoardColumnId }, cancellationToken);
		if (column == null)
			throw new BadRequestException($"{nameof(request.BoardColumnId)} is invalid");

		var initiatorUserRole = _boardService.GetRole(initiatorUserId, column.BoardId);
		if (initiatorUserRole == null ||
		    (initiatorUserRole != BoardRole.Admin && initiatorUserRole != BoardRole.Worker))
			throw new ForbiddenAccessException();

		var item = new BoardColumnItem
		{
			Title = request.Title,
			Description = request.Description
		};
		column.Items.Add(item);

		item.AddDomainEvent(new BoardColumnItemCreatedEvent(item, initiatorUserId, column.BoardId));

		await _context.SaveChangesAsync(cancellationToken);

		return item.Id;
	}
}