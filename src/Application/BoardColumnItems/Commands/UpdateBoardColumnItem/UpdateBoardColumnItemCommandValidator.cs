using FluentValidation;

namespace KanbanDesk.Application.BoardColumnItems.Commands.UpdateBoardColumnItem;

public class UpdateBoardColumnItemCommandValidator : AbstractValidator<UpdateBoardColumnItemCommand>
{
	public UpdateBoardColumnItemCommandValidator()
	{
		RuleFor(command => command.Id)
			.NotEmpty()
			.WithMessage($"{nameof(UpdateBoardColumnItemCommand.Id)} is required.");

		RuleFor(command => command.BoardColumnId)
			.NotEmpty()
			.WithMessage($"{nameof(UpdateBoardColumnItemCommand.BoardColumnId)} is required.");

		RuleFor(command => command.Title)
			.NotEmpty()
			.WithMessage($"{nameof(UpdateBoardColumnItemCommand.Title)} is required.")
			.MaximumLength(200)
			.WithMessage("Title must not exceed 200 characters.");
	}
}