using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.BoardColumnItems.Commands.UpdateBoardColumnItem;

public record UpdateBoardColumnItemCommand(Guid Id, Guid BoardColumnId, string Title, string Description,
	bool IsArchived) : IRequest;

public class UpdateCardCommandHandler : IRequestHandler<UpdateBoardColumnItemCommand>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public UpdateCardCommandHandler(IApplicationDbContext context, ICurrentUserService currentUserService,
		IBoardService boardService)
	{
		_context = context;
		_currentUserService = currentUserService;
		_boardService = boardService;
	}

	public async Task<Unit> Handle(UpdateBoardColumnItemCommand request, CancellationToken cancellationToken)
	{
		var initiatorUserId = _currentUserService.UserId!;

		var item = await _context.BoardColumnItems.FindAsync(new object?[] { request.Id }, cancellationToken);
		if (item == null)
			throw new NotFoundException(nameof(BoardColumnItem), request.Id);

		var column = await _context.BoardColumns.FindAsync(new object?[] { item.BoardColumnId }, cancellationToken);

		var initiatorUserRole = _boardService.GetRole(initiatorUserId, column!.BoardId);
		if (initiatorUserRole == null ||
		    (initiatorUserRole != BoardRole.Admin && initiatorUserRole != BoardRole.Worker))
			throw new ForbiddenAccessException();

		if (item.BoardColumnId != request.BoardColumnId)
		{
			var destinationColumn =
				await _context.BoardColumns.FindAsync(new object?[] { request.BoardColumnId }, cancellationToken);
			if (destinationColumn == null || destinationColumn.BoardId != column.BoardId)
				throw new BadRequestException();
			item.AddDomainEvent(new BoardColumnItemMovedEvent(item, initiatorUserId, column.BoardId));
		}

		if (item.IsArchived == false && request.IsArchived)
			item.AddDomainEvent(new BoardColumnItemArchivedEvent(item, initiatorUserId, column.BoardId));

		if (item.IsArchived && request.IsArchived == false)
			item.AddDomainEvent(new BoardColumnItemUnarchivedEvent(item, initiatorUserId, column.BoardId));

		item.Title = request.Title;
		item.Description = request.Description;
		item.BoardColumnId = request.BoardColumnId;
		item.IsArchived = request.IsArchived;

		await _context.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}