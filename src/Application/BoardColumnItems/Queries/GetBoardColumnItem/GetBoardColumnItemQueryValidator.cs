using FluentValidation;

namespace KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItem;

public class GetBoardColumnItemQueryValidator : AbstractValidator<GetBoardColumnItemQuery>
{
	public GetBoardColumnItemQueryValidator()
	{
		RuleFor(t => t.Id)
			.NotEmpty()
			.WithMessage($"{nameof(GetBoardColumnItemQuery.Id)} is required.");
	}
}