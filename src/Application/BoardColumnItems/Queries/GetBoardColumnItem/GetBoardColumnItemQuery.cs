using AutoMapper;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using MediatR;

namespace KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItem;

public record GetBoardColumnItemQuery(Guid Id) : IRequest<BoardColumnItemVm>;

public class GetCardQueryHandler : IRequestHandler<GetBoardColumnItemQuery, BoardColumnItemVm>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IMapper _mapper;

	public GetCardQueryHandler(IApplicationDbContext context, IBoardService boardService,
		ICurrentUserService currentUserService, IMapper mapper)
	{
		_context = context;
		_boardService = boardService;
		_currentUserService = currentUserService;
		_mapper = mapper;
	}

	public async Task<BoardColumnItemVm> Handle(GetBoardColumnItemQuery request, CancellationToken cancellationToken)
	{
		var item = await _context.BoardColumnItems.FindAsync(new object?[] { request.Id }, cancellationToken);
		if (item == null)
			throw new NotFoundException(nameof(BoardColumnItem), request.Id);

		var column = await _context.BoardColumns.FindAsync(new object?[] { item.BoardColumnId }, cancellationToken);

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, column!.BoardId) == false)
			throw new ForbiddenAccessException();

		return _mapper.Map<BoardColumnItem, BoardColumnItemVm>(item);
	}
}