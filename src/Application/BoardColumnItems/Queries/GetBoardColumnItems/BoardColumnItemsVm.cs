namespace KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItems;

public class BoardColumnItemsVm
{
	public required IList<BoardColumnItemDto> Items { get; set; } = new List<BoardColumnItemDto>();
}