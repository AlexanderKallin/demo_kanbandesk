using System.Linq.Expressions;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItems;

public record GetBoardColumnItemsQuery(Guid ColumnId, bool IncludeArchived) : IRequest<BoardColumnItemsVm>;

public class GetBoardColumnItemsQueryHandler : IRequestHandler<GetBoardColumnItemsQuery, BoardColumnItemsVm>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IMapper _mapper;

	public GetBoardColumnItemsQueryHandler(IApplicationDbContext context, IBoardService boardService,
		ICurrentUserService currentUserService, IMapper mapper)
	{
		_context = context;
		_boardService = boardService;
		_currentUserService = currentUserService;
		_mapper = mapper;
	}

	public async Task<BoardColumnItemsVm> Handle(GetBoardColumnItemsQuery request, CancellationToken cancellationToken)
	{
		var column = await _context.BoardColumns.FindAsync(new object?[] { request.ColumnId }, cancellationToken);
		if (column == null)
			throw new BadRequestException($"{nameof(request.ColumnId)} is invalid");

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, column.BoardId) == false)
			throw new ForbiddenAccessException();

		Expression<Func<BoardColumnItem, bool>> expression = item => item.BoardColumnId == request.ColumnId;

		if (request.IncludeArchived == false)
			expression = item => item.BoardColumnId == request.ColumnId && item.IsArchived == false;

		return new BoardColumnItemsVm
		{
			Items = await _context.BoardColumnItems
				.AsNoTracking()
				.Where(expression)
				.ProjectTo<BoardColumnItemDto>(_mapper.ConfigurationProvider)
				.ToListAsync(cancellationToken)
		};
	}
}