using FluentValidation;

namespace KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItems;

public class GetBoardColumnItemsQueryValidator : AbstractValidator<GetBoardColumnItemsQuery>
{
	public GetBoardColumnItemsQueryValidator()
	{
		RuleFor(t => t.ColumnId)
			.NotEmpty()
			.WithMessage($"{nameof(GetBoardColumnItemsQuery.ColumnId)} is required.");
	}
}