using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.Boards.Commands.CreateBoard;

public record CreateBoardCommand(string Title) : IRequest<Guid>;

public class CreateBoardCommandHandler : IRequestHandler<CreateBoardCommand, Guid>
{
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public CreateBoardCommandHandler(IApplicationDbContext context, ICurrentUserService currentUserService)
	{
		_context = context;
		_currentUserService = currentUserService;
	}

	public async Task<Guid> Handle(CreateBoardCommand request, CancellationToken cancellationToken)
	{
		var board = new Board { Title = request.Title };

		board.AddDomainEvent(new BoardCreatedEvent(board, _currentUserService.UserId!));

		await _context.Boards.AddAsync(board, cancellationToken);
		await _context.SaveChangesAsync(cancellationToken);

		return board.Id;
	}
}