using FluentValidation;

namespace KanbanDesk.Application.Boards.Commands.CreateBoard;

public class CreateBoardCommandValidator : AbstractValidator<CreateBoardCommand>
{
	public CreateBoardCommandValidator()
	{
		RuleFor(command => command.Title)
			.NotEmpty()
			.WithMessage($"{nameof(CreateBoardCommand.Title)} is required.")
			.MaximumLength(200)
			.WithMessage($"{nameof(CreateBoardCommand.Title)} must not exceed 200 characters.");
	}
}