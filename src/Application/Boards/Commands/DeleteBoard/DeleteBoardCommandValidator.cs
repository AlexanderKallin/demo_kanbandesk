using FluentValidation;

namespace KanbanDesk.Application.Boards.Commands.DeleteBoard;

public class DeleteBoardCommandValidator : AbstractValidator<DeleteBoardCommand>
{
	public DeleteBoardCommandValidator()
	{
		RuleFor(command => command.Id)
			.NotEmpty()
			.WithMessage($"{nameof(DeleteBoardCommand.Id)} is required.");
	}
}