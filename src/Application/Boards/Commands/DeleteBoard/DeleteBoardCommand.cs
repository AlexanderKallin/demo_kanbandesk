using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using MediatR;

namespace KanbanDesk.Application.Boards.Commands.DeleteBoard;

public record DeleteBoardCommand(Guid Id) : IRequest;

public class DeleteBoardCommandHandler : IRequestHandler<DeleteBoardCommand>
{
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public DeleteBoardCommandHandler(IApplicationDbContext context, ICurrentUserService currentUserService)
	{
		_context = context;
		_currentUserService = currentUserService;
	}

	public async Task<Unit> Handle(DeleteBoardCommand request, CancellationToken cancellationToken)
	{
		var board = await _context.Boards.FindAsync(new object[] { request.Id }, cancellationToken);

		if (board == null)
			throw new NotFoundException(nameof(Board), request.Id);

		if (board.CreatedBy != _currentUserService.UserId)
			throw new ForbiddenAccessException();

		_context.Boards.Remove(board);

		await _context.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}