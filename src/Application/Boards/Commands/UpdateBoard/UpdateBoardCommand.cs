using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.Boards.Commands.UpdateBoard;

public record UpdateBoardCommand(Guid Id, string Title) : IRequest;

public class UpdateBoardCommandHandler : IRequestHandler<UpdateBoardCommand>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;

	public UpdateBoardCommandHandler(IApplicationDbContext context, IBoardService boardService,
		ICurrentUserService currentUserService)
	{
		_context = context;
		_boardService = boardService;
		_currentUserService = currentUserService;
	}

	public async Task<Unit> Handle(UpdateBoardCommand request, CancellationToken cancellationToken)
	{
		var board = await _context.Boards.FindAsync(new object[] { request.Id }, cancellationToken);
		if (board == null)
			throw new NotFoundException(nameof(Board), request.Id);

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, request.Id, BoardRole.Admin) == false)
			throw new ForbiddenAccessException();

		board.AddDomainEvent(new BoardTitleChangedEvent(board, _currentUserService.UserId!, board.Title));

		board.Title = request.Title;

		await _context.SaveChangesAsync(cancellationToken);

		return Unit.Value;
	}
}