using FluentValidation;

namespace KanbanDesk.Application.Boards.Commands.UpdateBoard;

public class UpdateBoardCommandValidator : AbstractValidator<UpdateBoardCommand>
{
	public UpdateBoardCommandValidator()
	{
		RuleFor(command => command.Id)
			.NotEmpty()
			.WithMessage($"{nameof(UpdateBoardCommand.Id)} is required.");

		RuleFor(command => command.Title)
			.NotEmpty()
			.WithMessage($"{nameof(UpdateBoardCommand.Title)} is required.")
			.MaximumLength(200)
			.WithMessage($"{nameof(UpdateBoardCommand.Title)} must not exceed 200 characters.");
	}
}