using FluentValidation;

namespace KanbanDesk.Application.Boards.Queries.GetBoard;

public class GetBoardQueryValidator : AbstractValidator<GetBoardQuery>
{
	public GetBoardQueryValidator()
	{
		RuleFor(query => query.BoardId)
			.NotEmpty()
			.WithMessage($"{nameof(GetBoardQuery.BoardId)} is required.");
	}
}