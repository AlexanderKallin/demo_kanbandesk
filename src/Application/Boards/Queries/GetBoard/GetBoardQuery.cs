using AutoMapper;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace KanbanDesk.Application.Boards.Queries.GetBoard;

public record GetBoardQuery(Guid BoardId) : IRequest<BoardVm>;

public class GetBoardQueryHandler : IRequestHandler<GetBoardQuery, BoardVm>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IMapper _mapper;

	public GetBoardQueryHandler(IApplicationDbContext context, IBoardService boardService,
		ICurrentUserService currentUserService, IMapper mapper)
	{
		_context = context;
		_boardService = boardService;
		_currentUserService = currentUserService;
		_mapper = mapper;
	}

	public async Task<BoardVm> Handle(GetBoardQuery request, CancellationToken cancellationToken)
	{
		if (_boardService.IsBoardExists(request.BoardId) == false)
			throw new NotFoundException(nameof(Board), request.BoardId);

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, request.BoardId) == false)
			throw new ForbiddenAccessException();

		var board = await _context.Boards
			.AsNoTracking()
			.Include(board => board.BoardColumns.Where(column => column.IsArchived == false))
			.ThenInclude(boardColumn => boardColumn.Items.Where(item => item.IsArchived == false))
			.AsSplitQuery()
			.FirstAsync(board => board.Id == request.BoardId, cancellationToken);

		return _mapper.Map<Board, BoardVm>(board!);
	}
}