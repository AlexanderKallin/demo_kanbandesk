using AutoMapper;
using KanbanDesk.Application.Boards.DTOs;
using KanbanDesk.Application.Common.Mappings;
using KanbanDesk.Domain.Entities;

namespace KanbanDesk.Application.Boards.Queries.GetBoards;

public class BoardDto : IMapFrom<Board>
{
	public Guid Id { get; set; }
	public string Title { get; set; } = null!;
	public IList<BoardColumnDto> BoardColumns { get; set; } = new List<BoardColumnDto>();

	public void Mapping(Profile profile)
	{
		profile.CreateMap<Board, BoardDto>()
			.ForMember(destination => destination.Id,
				options => options.MapFrom(source => source.Id))
			.ForMember(destination => destination.Title,
				options => options.MapFrom(source => source.Title))
			.ForMember(destination => destination.BoardColumns,
				options => options.MapFrom(source => source.BoardColumns));
	}
}