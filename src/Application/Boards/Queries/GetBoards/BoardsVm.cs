namespace KanbanDesk.Application.Boards.Queries.GetBoards;

public class BoardsVm
{
	public required IList<BoardDto> Boards { get; set; } = new List<BoardDto>();
}