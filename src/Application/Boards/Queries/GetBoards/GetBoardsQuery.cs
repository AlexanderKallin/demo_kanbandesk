using AutoMapper;
using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace KanbanDesk.Application.Boards.Queries.GetBoards;

public record GetBoardsQuery : IRequest<BoardsVm>;

public class GetBoardsQueryHandler : IRequestHandler<GetBoardsQuery, BoardsVm>
{
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IMapper _mapper;

	public GetBoardsQueryHandler(IApplicationDbContext context, ICurrentUserService currentUserService, IMapper mapper)
	{
		_context = context;
		_currentUserService = currentUserService;
		_mapper = mapper;
	}

	public async Task<BoardsVm> Handle(GetBoardsQuery request, CancellationToken cancellationToken)
	{
		var availableBoardsId = await _context.UserBoardRoles
			.AsNoTracking()
			.Where(boardUserRole => boardUserRole.UserId == _currentUserService.UserId!)
			.Select(boardUserRole => boardUserRole.BoardId)
			.ToArrayAsync(cancellationToken);

		var boards = await _context.Boards
			.AsNoTracking()
			.Include(board => board.BoardColumns.Where(column => column.IsArchived == false))
			.ThenInclude(boardColumn => boardColumn.Items.Where(item => item.IsArchived == false))
			.AsSplitQuery()
			.Where(board => availableBoardsId.Contains(board.Id))
			.ToListAsync(cancellationToken);

		return new BoardsVm
		{
			Boards = _mapper.Map<List<Board>, List<BoardDto>>(boards)
		};
	}
}