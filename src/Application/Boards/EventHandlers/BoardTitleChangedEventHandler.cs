using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.Boards.EventHandlers;

public class BoardTitleChangedEventHandler : INotificationHandler<BoardTitleChangedEvent>
{
	private readonly IApplicationDbContext _context;

	public BoardTitleChangedEventHandler(IApplicationDbContext context)
	{
		_context = context;
	}

	public async Task Handle(BoardTitleChangedEvent notification, CancellationToken cancellationToken)
	{
		var eventLogMessage = new BoardEventLogMessage
		{
			BoardId = notification.Board.Id,
			InitiatorUserId = notification.InitiatorUserId,
			DateTime = DateTime.Now,
			Message = $"renamed this board (from {notification.OldTitle})"
		};
		await _context.BoardEventLogMessages.AddAsync(eventLogMessage, cancellationToken);

		await _context.SaveChangesAsync(cancellationToken);
	}
}