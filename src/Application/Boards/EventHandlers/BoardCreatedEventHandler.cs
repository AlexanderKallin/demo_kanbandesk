using KanbanDesk.Application.Common.Interfaces;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using KanbanDesk.Domain.Events;
using MediatR;

namespace KanbanDesk.Application.Boards.EventHandlers;

public class BoardCreatedEventHandler : INotificationHandler<BoardCreatedEvent>
{
	private readonly IApplicationDbContext _context;

	public BoardCreatedEventHandler(IApplicationDbContext context)
	{
		_context = context;
	}

	public async Task Handle(BoardCreatedEvent notification, CancellationToken cancellationToken)
	{
		var userBoardRole = new UserBoardRole
		{
			UserId = notification.UserInitiatorId,
			BoardId = notification.Board.Id,
			BoardRole = BoardRole.Admin
		};
		await _context.UserBoardRoles.AddAsync(userBoardRole, cancellationToken);

		var eventLogMessage = new BoardEventLogMessage
		{
			BoardId = notification.Board.Id,
			InitiatorUserId = notification.UserInitiatorId,
			DateTime = DateTime.Now,
			Message = "created this board"
		};
		await _context.BoardEventLogMessages.AddAsync(eventLogMessage, cancellationToken);

		await _context.SaveChangesAsync(cancellationToken);
	}
}