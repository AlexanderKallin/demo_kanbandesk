using AutoMapper;
using KanbanDesk.Application.Common.Mappings;
using KanbanDesk.Domain.Entities;

namespace KanbanDesk.Application.Boards.DTOs;

public class BoardColumnDto : IMapFrom<BoardColumn>
{
	public Guid Id { get; set; }
	public Guid BoardId { get; set; }
	public string Title { get; set; } = null!;
	public bool IsArchived { get; set; }
	public IList<BoardColumnItemDto> Items { get; set; } = new List<BoardColumnItemDto>();

	public void Mapping(Profile profile)
	{
		profile.CreateMap<BoardColumn, BoardColumnDto>()
			.ForMember(destination => destination.Id,
				options => options.MapFrom(source => source.Id))
			.ForMember(destination => destination.BoardId,
				options => options.MapFrom(source => source.BoardId))
			.ForMember(destination => destination.Title,
				options => options.MapFrom(source => source.Title))
			.ForMember(destination => destination.IsArchived,
				options => options.MapFrom(source => source.IsArchived))
			.ForMember(destination => destination.Items,
				options => options.MapFrom(source => source.Items));
	}
}