using AutoMapper;
using KanbanDesk.Application.Common.Mappings;
using KanbanDesk.Domain.Entities;

namespace KanbanDesk.Application.Boards.DTOs;

public class BoardColumnItemDto : IMapFrom<BoardColumnItem>
{
	public Guid Id { get; set; }
	public Guid BoardColumnId { set; get; }
	public string Title { get; set; } = null!;
	public string? Description { get; set; }
	public bool IsArchived { get; set; }

	public void Mapping(Profile profile)
	{
		profile.CreateMap<BoardColumnItem, BoardColumnItemDto>()
			.ForMember(destination => destination.Id,
				options => options.MapFrom(source => source.Id))
			.ForMember(destination => destination.BoardColumnId,
				options => options.MapFrom(source => source.BoardColumnId))
			.ForMember(destination => destination.Title,
				options => options.MapFrom(source => source.Title))
			.ForMember(destination => destination.Description,
				options => options.MapFrom(source => source.Description))
			.ForMember(destination => destination.IsArchived,
				options => options.MapFrom(source => source.IsArchived));
	}
}