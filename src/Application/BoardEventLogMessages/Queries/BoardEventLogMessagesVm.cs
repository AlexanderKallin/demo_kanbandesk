namespace KanbanDesk.Application.BoardEventLogMessages.Queries;

public class BoardEventLogMessagesVm
{
	public List<BoardEventLogMessageDto> Messages { get; set; } = new();
}