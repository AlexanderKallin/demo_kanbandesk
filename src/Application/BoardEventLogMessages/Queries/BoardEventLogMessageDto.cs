using AutoMapper;
using KanbanDesk.Application.Common.Mappings;
using KanbanDesk.Domain.Entities;

namespace KanbanDesk.Application.BoardEventLogMessages.Queries;

public class BoardEventLogMessageDto : IMapFrom<BoardEventLogMessage>
{
	public int Id { get; set; }
	public required Guid BoardId { get; set; }
	public required string InitiatorUserId { get; set; } = null!;
	public required DateTime DateTime { get; set; }
	public string? Message { get; set; }
	public Guid? SubjectId { get; set; }

	public void Mapping(Profile profile)
	{
		profile.CreateMap<BoardEventLogMessage, BoardEventLogMessageDto>()
			.ForMember(destination => destination.Id,
				options => options.MapFrom(source => source.Id))
			.ForMember(destination => destination.BoardId,
				options => options.MapFrom(source => source.BoardId))
			.ForMember(destination => destination.InitiatorUserId,
				options => options.MapFrom(source => source.InitiatorUserId))
			.ForMember(destination => destination.DateTime,
				options => options.MapFrom(source => source.DateTime))
			.ForMember(destination => destination.Message,
				options => options.MapFrom(source => source.Message))
			.ForMember(destination => destination.SubjectId,
				options => options.MapFrom(source => source.SubjectId));
	}
}