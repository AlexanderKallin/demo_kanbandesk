using AutoMapper;
using AutoMapper.QueryableExtensions;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace KanbanDesk.Application.BoardEventLogMessages.Queries;

public record GetBoardEventLogMessagesQuery(Guid BoardId) : IRequest<BoardEventLogMessagesVm>;

public class
	GetBoardEventLogMessagesQueryHandler : IRequestHandler<GetBoardEventLogMessagesQuery, BoardEventLogMessagesVm>
{
	private readonly IBoardService _boardService;
	private readonly IApplicationDbContext _context;
	private readonly ICurrentUserService _currentUserService;
	private readonly IMapper _mapper;

	public GetBoardEventLogMessagesQueryHandler(IBoardService boardService, ICurrentUserService currentUserService,
		IApplicationDbContext context, IMapper mapper)
	{
		_boardService = boardService;
		_currentUserService = currentUserService;
		_context = context;
		_mapper = mapper;
	}

	public async Task<BoardEventLogMessagesVm> Handle(GetBoardEventLogMessagesQuery request,
		CancellationToken cancellationToken)
	{
		if (_boardService.IsBoardExists(request.BoardId) == false)
			throw new BadRequestException($"{nameof(request.BoardId)} is invalid");

		if (_boardService.IsUserBoardRoleExists(_currentUserService.UserId!, request.BoardId) == false)
			throw new ForbiddenAccessException();

		return new BoardEventLogMessagesVm
		{
			Messages = await _context.BoardEventLogMessages
				.AsNoTracking()
				.Where(message => message.BoardId == request.BoardId)
				.ProjectTo<BoardEventLogMessageDto>(_mapper.ConfigurationProvider)
				.ToListAsync(cancellationToken)
		};
	}
}