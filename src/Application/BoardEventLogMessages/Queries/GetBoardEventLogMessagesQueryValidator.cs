using FluentValidation;

namespace KanbanDesk.Application.BoardEventLogMessages.Queries;

public class GetBoardEventLogMessagesQueryValidator : AbstractValidator<GetBoardEventLogMessagesQuery>
{
	public GetBoardEventLogMessagesQueryValidator()
	{
		RuleFor(t => t.BoardId)
			.NotEmpty()
			.WithMessage($"{nameof(GetBoardEventLogMessagesQuery.BoardId)} is required.");
	}
}