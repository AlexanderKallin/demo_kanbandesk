using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests;

using static Testing;

public abstract class KanbanDeskTestFixture : BaseTestFixture
{
	[SetUp]
	public new async Task TestSetUp()
	{
		await base.TestSetUp();

		DefaultUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(DefaultUser);
	}

	protected ApplicationUser DefaultUser = null!;

	protected async Task AssignBoardRoleAsync(ApplicationUser applicationUser, Board board, BoardRole role)
	{
		if (applicationUser == null)
			throw new NullReferenceException(nameof(applicationUser));

		if (board == null)
			throw new NullReferenceException(nameof(board));

		var userBoardRole = new UserBoardRole { BoardId = board.Id, UserId = applicationUser.Id, BoardRole = role };
		await AddAsync(userBoardRole);
	}

	protected async Task CreateBoard(Board board)
	{
		if (GetCurrentUserId() == null)
			throw new Exception("Cant create board. CurrentUserId == null.");

		await AddAsync(board);
		await AssignBoardRoleAsync(DefaultUser, board, BoardRole.Admin);
	}

	protected async Task<ApplicationUser> CreateAndRunAsUserWithRole(Board board, BoardRole role)
	{
		var anotherUser = await CreateIdentityUserAsync();
		await AssignBoardRoleAsync(anotherUser, board, role);
		await RunAsUserAsync(anotherUser);

		return anotherUser;
	}
}