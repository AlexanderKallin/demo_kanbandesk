using FluentAssertions;
using KanbanDesk.Application.BoardColumnItems.Commands.DeleteBoardColumnItem;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumnItems.Commands;

using static Testing;

public class DeleteBoardColumnItemTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_boardColumnItem = new BoardColumnItem { Title = "Item #1" };
		_boardColumn = new BoardColumn
			{ Title = "BoardColumn #1", Items = new List<BoardColumnItem> { _boardColumnItem } };
		_board = new Board
		{
			Title = "Board #1",
			BoardColumns = new List<BoardColumn>
			{
				_boardColumn
			}
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;
	private BoardColumn _boardColumn = null!;
	private BoardColumnItem _boardColumnItem = null!;

	[Test]
	public async Task ShouldRequireMinimalFields()
	{
		var invalidBoardColumnItemId = Guid.Empty;

		var command = new DeleteBoardColumnItemCommand(invalidBoardColumnItemId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardColumnItemToExists()
	{
		var nonExistentBoardColumnItemId = Guid.NewGuid();

		var command = new DeleteBoardColumnItemCommand(nonExistentBoardColumnItemId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminOrWorkerRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new DeleteBoardColumnItemCommand(_boardColumnItem.Id);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldDeleteBoardColumnItem()
	{
		await SendAsync(new DeleteBoardColumnItemCommand(_boardColumnItem.Id));

		var item = await FindAsync<BoardColumnItem>(_boardColumnItem.Id);

		item.Should().BeNull();
	}
}