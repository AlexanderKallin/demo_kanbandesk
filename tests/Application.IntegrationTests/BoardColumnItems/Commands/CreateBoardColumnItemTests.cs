using FluentAssertions;
using KanbanDesk.Application.BoardColumnItems.Commands.CreateBoardColumnItem;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumnItems.Commands;

using static Testing;

public class CreateBoardColumnItemTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_column = new BoardColumn { Title = "BoardColumn #1" };
		_board = new Board
		{
			Title = "Board #1",
			BoardColumns = new List<BoardColumn>
			{
				_column
			}
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;
	private BoardColumn _column = null!;

	[Test]
	public async Task ShouldRequireMinimalFields()
	{
		var invalidBoardColumnId = Guid.Empty;
		var invalidTitle = string.Empty;

		var command = new CreateBoardColumnItemCommand(invalidBoardColumnId, invalidTitle, string.Empty);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardColumnToExists()
	{
		var nonExistentBoardColumnId = Guid.NewGuid();

		var command = new CreateBoardColumnItemCommand(nonExistentBoardColumnId, "Item #1", string.Empty);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminOrWorkerRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new CreateBoardColumnItemCommand(_column.Id, "Item #1", string.Empty);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldCreateBoardColumnItem()
	{
		var command = new CreateBoardColumnItemCommand(_column.Id, "Item #1", string.Empty);

		var createdItemId = await SendAsync(command);

		var createdItem = await FindAsync<BoardColumnItem>(createdItemId);

		createdItem.Should().NotBeNull();
		createdItem!.Title.Should().Be(command.Title);
		createdItem.Created.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		createdItem.CreatedBy.Should().Be(DefaultUser.Id);
		createdItem.LastModified.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		createdItem.LastModifiedBy.Should().Be(DefaultUser.Id);
	}
}