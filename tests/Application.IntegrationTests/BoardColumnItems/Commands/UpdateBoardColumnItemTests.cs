using FluentAssertions;
using KanbanDesk.Application.BoardColumnItems.Commands.UpdateBoardColumnItem;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumnItems.Commands;

using static Testing;

public class UpdateBoardColumnItemTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_boardColumnItem = new BoardColumnItem { Title = "Item #1" };
		_boardColumn = new BoardColumn
			{ Title = "BoardColumn #1", Items = new List<BoardColumnItem> { _boardColumnItem } };
		_board = new Board
		{
			Title = "Board #1",
			BoardColumns = new List<BoardColumn>
			{
				_boardColumn
			}
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;
	private BoardColumn _boardColumn = null!;
	private BoardColumnItem _boardColumnItem = null!;

	[Test]
	public async Task ShouldRequireMinimalFields()
	{
		var invalidBoardColumnItemId = Guid.Empty;
		var invalidBoardColumnId = Guid.Empty;
		var invalidTitle = string.Empty;

		var command = new UpdateBoardColumnItemCommand(invalidBoardColumnItemId, invalidBoardColumnId, invalidTitle,
			string.Empty, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardColumnItemToExists()
	{
		var nonExistentBoardColumnItemId = Guid.NewGuid();

		var command = new UpdateBoardColumnItemCommand(nonExistentBoardColumnItemId, _boardColumn.Id, "Updated item #1",
			string.Empty, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminOrWorkerRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new UpdateBoardColumnItemCommand(_boardColumnItem.Id, _boardColumn.Id, "Updated item #1",
			string.Empty, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldUpdateBoardColumnItem()
	{
		var command = new UpdateBoardColumnItemCommand(_boardColumnItem.Id, _boardColumn.Id, "Updated item #1",
			string.Empty, true);

		await SendAsync(command);

		var item = await FindAsync<BoardColumnItem>(command.Id);

		item.Should().NotBeNull();
		item!.BoardColumnId.Should().Be(_boardColumnItem.BoardColumnId);
		item.Title.Should().Be(command.Title);
		item.Description.Should().Be(command.Description);
		item.IsArchived.Should().Be(command.IsArchived);
		item.Created.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		item.CreatedBy.Should().Be(DefaultUser.Id);
		item.LastModified.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		item.LastModifiedBy.Should().Be(DefaultUser.Id);
	}
}