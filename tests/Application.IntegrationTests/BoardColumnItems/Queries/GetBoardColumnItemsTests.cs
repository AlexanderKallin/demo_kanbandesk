using FluentAssertions;
using KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItems;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumnItems.Queries;

using static Testing;

public class GetBoardColumnItemsTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_boardColumn = new BoardColumn
		{
			Title = "BoardColumn #1",
			Items = new List<BoardColumnItem>
			{
				new() { Title = "Item #1", Description = "Item #1 description" },
				new() { Title = "Item #2", IsArchived = true }
			}
		};
		_board = new Board { Title = "Board #1", BoardColumns = new List<BoardColumn> { _boardColumn } };
		await CreateBoard(_board);
	}

	private Board _board = null!;
	private BoardColumn _boardColumn = null!;

	[Test]
	public async Task ShouldRequireMinimalFields()
	{
		var invalidColumnItemId = Guid.Empty;

		var command = new GetBoardColumnItemsQuery(invalidColumnItemId, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardColumnToExists()
	{
		var nonExistentBoardColumnId = Guid.NewGuid();

		var command = new GetBoardColumnItemsQuery(nonExistentBoardColumnId, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveRole()
	{
		var anotherUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(anotherUser);

		var command = new GetBoardColumnItemsQuery(_boardColumn.Id, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldReturnAllBoardColumnItemsIncludeArchived()
	{
		var queryResult = await SendAsync(new GetBoardColumnItemsQuery(_boardColumn.Id, true));

		queryResult.Items.Should().HaveCount(_boardColumn.Items.Count);

		var firstReturnedItem = queryResult.Items.First();
		var firstExistentItem = _boardColumn.Items.First();
		firstReturnedItem.Id.Should().Be(firstExistentItem.Id);
		firstReturnedItem.BoardColumnId.Should().Be(firstExistentItem.BoardColumnId);
		firstReturnedItem.Title.Should().Be(firstExistentItem.Title);
		firstReturnedItem.Description.Should().Be(firstExistentItem.Description);
		firstReturnedItem.IsArchived.Should().Be(firstExistentItem.IsArchived);

		var secondReturnedItem = queryResult.Items.Skip(1).First();
		var secondExistentItem = _boardColumn.Items.Skip(1).First();
		secondReturnedItem.Id.Should().Be(secondExistentItem.Id);
		secondReturnedItem.BoardColumnId.Should().Be(secondExistentItem.BoardColumnId);
		secondReturnedItem.Title.Should().Be(secondExistentItem.Title);
		secondReturnedItem.Description.Should().Be(secondExistentItem.Description);
		secondReturnedItem.IsArchived.Should().Be(secondExistentItem.IsArchived);
	}

	[Test]
	public async Task ShouldReturnOnlyNotArchivedBoardColumnItems()
	{
		var queryResult = await SendAsync(new GetBoardColumnItemsQuery(_boardColumn.Id, false));

		queryResult.Items.Should().HaveCount(1);

		var returnedItem = queryResult.Items.First();
		var existentItem = _boardColumn.Items.First();
		returnedItem.Id.Should().Be(existentItem.Id);
		returnedItem.BoardColumnId.Should().Be(existentItem.BoardColumnId);
		returnedItem.Title.Should().Be(existentItem.Title);
		returnedItem.Description.Should().Be(existentItem.Description);
		returnedItem.IsArchived.Should().BeFalse();
	}
}