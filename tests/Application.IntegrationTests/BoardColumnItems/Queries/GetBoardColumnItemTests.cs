using FluentAssertions;
using KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItem;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumnItems.Queries;

using static Testing;

public class GetBoardColumnItemTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_boardColumnItem = new BoardColumnItem { Title = "Item #1" };
		_boardColumn = new BoardColumn
		{
			Title = "BoardColumn #1",
			Items = new List<BoardColumnItem> { _boardColumnItem }
		};
		_board = new Board { Title = "Board #1", BoardColumns = new List<BoardColumn> { _boardColumn } };
		await CreateBoard(_board);
	}

	private Board _board = null!;
	private BoardColumn _boardColumn = null!;
	private BoardColumnItem _boardColumnItem = null!;

	[Test]
	public async Task ShouldRequireMinimalFields()
	{
		var invalidBoardColumnItemId = Guid.Empty;

		var command = new GetBoardColumnItemQuery(invalidBoardColumnItemId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardColumnItemToExists()
	{
		var nonExistentBoardColumnItemId = Guid.NewGuid();

		var command = new GetBoardColumnItemQuery(nonExistentBoardColumnItemId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveRole()
	{
		var anotherUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(anotherUser);

		var command = new GetBoardColumnItemQuery(_boardColumnItem.Id);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldReturnBoardColumnItem()
	{
		var item = await SendAsync(new GetBoardColumnItemQuery(_boardColumnItem.Id));

		item.Id.Should().Be(_boardColumnItem.Id);
		item.BoardColumnId.Should().Be(_boardColumnItem.BoardColumnId);
		item.Title.Should().Be(_boardColumnItem.Title);
		item.Description.Should().Be(_boardColumnItem.Description);
		item.IsArchived.Should().Be(_boardColumnItem.IsArchived);
	}
}