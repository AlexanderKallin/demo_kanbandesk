﻿using KanbanDesk.Domain.Entities;
using KanbanDesk.Infrastructure.Identity;
using KanbanDesk.Infrastructure.Persistence;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Respawn;
using Respawn.Graph;

namespace KanbanDesk.Application.IntegrationTests;

[SetUpFixture]
public class Testing
{
	private static WebApplicationFactory<Program> _factory = null!;
	private static IConfiguration _configuration = null!;
	private static IServiceScopeFactory _scopeFactory = null!;
	private static Respawner _respawner = null!;
	private static string? _currentUserId;
	private static string _connection = null!;

	[OneTimeSetUp]
	public async Task RunBeforeAnyTests()
	{
		_factory = new CustomWebApplicationFactory();
		_scopeFactory = _factory.Services.GetRequiredService<IServiceScopeFactory>();
		_configuration = _factory.Services.GetRequiredService<IConfiguration>();
		_connection = _configuration.GetConnectionString("DefaultConnection");
		_respawner = await Respawner.CreateAsync(_connection, new RespawnerOptions
		{
			TablesToIgnore = new Table[] { "__EFMigrationsHistory" }
		});
	}

	public static async Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
	{
		using var scope = _scopeFactory.CreateScope();

		var mediator = scope.ServiceProvider.GetRequiredService<ISender>();

		return await mediator.Send(request);
	}

	public static string? GetCurrentUserId()
	{
		return _currentUserId;
	}

	public static async Task<ApplicationUser> CreateIdentityUserAsync()
	{
		return await CreateIdentityUserAsync($"User-{Guid.NewGuid()}", "Testing1234!", Array.Empty<string>());
	}

	private static async Task<ApplicationUser> CreateIdentityUserAsync(string userName, string password, string[] roles)
	{
		using var scope = _scopeFactory.CreateScope();

		var userManager =
			scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

		ApplicationUser user = new() { UserName = userName, Email = userName };

		var result = await userManager.CreateAsync(user, password);

		if (roles.Any())
		{
			var roleManager =
				scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

			foreach (var role in roles)
				await roleManager.CreateAsync(new IdentityRole(role));

			await userManager.AddToRolesAsync(user, roles);
		}

		if (result.Succeeded == false)
		{
			var errors = string.Join(Environment.NewLine, result.ToApplicationResult().Errors);

			throw new Exception($"Unable to create {userName}.{Environment.NewLine}{errors}");
		}

		return user;
	}

	public static async Task<string> RunAsUserAsync(ApplicationUser applicationUser)
	{
		using var scope = _scopeFactory.CreateScope();

		var userManager =
			scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

		var user = await userManager.FindByIdAsync(applicationUser.Id);

		if (user == null)
			throw new Exception($"User with {nameof(applicationUser.Id)} {applicationUser.Id} not found");

		_currentUserId = user.Id;

		return _currentUserId;
	}

	public static async Task ResetState()
	{
		await _respawner.ResetAsync(_configuration.GetConnectionString("DefaultConnection"));

		_currentUserId = null;
	}

	public static async Task<TEntity?> FindAsync<TEntity>(params object[] keyValues)
		where TEntity : class
	{
		using var scope = _scopeFactory.CreateScope();

		var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

		return await context.FindAsync<TEntity>(keyValues);
	}

	public static async Task AddAsync<TEntity>(TEntity entity)
		where TEntity : class
	{
		using var scope = _scopeFactory.CreateScope();

		var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

		context.Add(entity);

		await context.SaveChangesAsync();
	}

	public static async Task<int> CountAsync<TEntity>() where TEntity : class
	{
		using var scope = _scopeFactory.CreateScope();

		var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

		return await context.Set<TEntity>().CountAsync();
	}

	[OneTimeTearDown]
	public void RunAfterAnyTests()
	{
	}
}