using FluentAssertions;
using KanbanDesk.Application.BoardColumns.Commands.UpdateBoardColumn;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumns.Commands;

using static Testing;

public class UpdateBoardColumnTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_column = new BoardColumn { Title = "Column #1" };
		_board = new Board
		{
			Title = "Board #1",
			BoardColumns = new List<BoardColumn>
			{
				_column
			}
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;
	private BoardColumn _column = null!;

	[Test]
	public async Task ShouldRequireMinimalFields()
	{
		var invalidColumnId = Guid.Empty;
		var invalidTitle = string.Empty;

		var command = new UpdateBoardColumnCommand(invalidColumnId, invalidTitle, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireColumnToExists()
	{
		var invalidColumnId = Guid.NewGuid();

		var command = new UpdateBoardColumnCommand(invalidColumnId, "Updated column #1", false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminOrWorkerRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new UpdateBoardColumnCommand(_column.Id, "Updated column #1", false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldUpdateBoardColumn()
	{
		var command = new UpdateBoardColumnCommand(_column.Id, "Updated column #1", true);
		await SendAsync(command);

		var column = await FindAsync<BoardColumn>(_column.Id);

		column.Should().NotBeNull();
		column!.Title.Should().Be(command.Title);
		column.IsArchived.Should().Be(command.IsArchived);
		column.LastModified.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		column.LastModifiedBy.Should().Be(DefaultUser.Id);
	}
}