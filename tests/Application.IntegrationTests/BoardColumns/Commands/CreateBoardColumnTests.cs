using FluentAssertions;
using KanbanDesk.Application.BoardColumns.Commands.CreateBoardColumn;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumns.Commands;

using static Testing;

public class CreateBoardColumnTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board { Title = "Board #1" };
		await CreateBoard(_board);
	}

	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidBoardId = Guid.Empty;
		var invalidTitle = string.Empty;

		var command = new CreateBoardColumnCommand(invalidBoardId, invalidTitle);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var invalidBoardId = Guid.NewGuid();

		var command = new CreateBoardColumnCommand(invalidBoardId, "Column #1");

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminOrWorkerRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new CreateBoardColumnCommand(_board.Id, "Column #1");

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldCreateBoardColumn()
	{
		var command = new CreateBoardColumnCommand(_board.Id, "Column #1");
		var boardColumnId = await SendAsync(command);

		var column = await FindAsync<BoardColumn>(boardColumnId);

		column.Should().NotBeNull();
		column!.BoardId.Should().Be(_board.Id);
		column.Title.Should().Be(command.Title);
		column.Created.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		column.CreatedBy.Should().Be(DefaultUser.Id);
		column.LastModified.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		column.LastModifiedBy.Should().Be(DefaultUser.Id);
	}
}