using FluentAssertions;
using KanbanDesk.Application.BoardColumns.Commands.DeleteBoardColumn;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumns.Commands;

using static Testing;

public class DeleteBoardColumnTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_column = new BoardColumn { Title = "Column #1" };
		_board = new Board
		{
			Title = "Board #1",
			BoardColumns = new List<BoardColumn>
			{
				_column
			}
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;
	private BoardColumn _column = null!;

	[Test]
	public async Task ShouldRequireMinimalFields()
	{
		var invalidColumnId = Guid.Empty;

		var command = new DeleteBoardColumnCommand(invalidColumnId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireColumnToExists()
	{
		var invalidColumnId = Guid.NewGuid();

		var command = new DeleteBoardColumnCommand(invalidColumnId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminOrWorkerRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new DeleteBoardColumnCommand(_column.Id);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldDeleteBoardColumn()
	{
		await SendAsync(new DeleteBoardColumnCommand(_column.Id));

		var boardColumn = await FindAsync<BoardColumn>(_column.Id);

		boardColumn.Should().BeNull();
	}
}