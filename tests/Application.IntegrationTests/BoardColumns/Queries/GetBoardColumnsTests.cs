using FluentAssertions;
using KanbanDesk.Application.BoardColumns.Queries.GetBoardColumns;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumns.Queries;

using static Testing;

public class GetBoardColumnsTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_card1 = new BoardColumnItem { Title = "BoardColumnItem #1" };
		_card2 = new BoardColumnItem { Title = "BoardColumnItem #2" };
		_card3 = new BoardColumnItem { Title = "BoardColumnItem #3" };
		_card4 = new BoardColumnItem { Title = "BoardColumnItem #4" };

		_boardColumn1 = new BoardColumn
			{ Title = "BoardColumn #1", Items = new List<BoardColumnItem> { _card1, _card2 } };
		_boardColumn2 = new BoardColumn
			{ Title = "BoardColumn #2", Items = new List<BoardColumnItem> { _card3, _card4 }, IsArchived = true };

		_board = new Board
			{ Title = "Board title", BoardColumns = new List<BoardColumn> { _boardColumn1, _boardColumn2 } };

		await CreateBoard(_board);
	}

	private BoardColumnItem _card1 = null!;
	private BoardColumnItem _card2 = null!;
	private BoardColumnItem _card3 = null!;
	private BoardColumnItem _card4 = null!;
	private BoardColumn _boardColumn1 = null!;
	private BoardColumn _boardColumn2 = null!;
	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimalFields()
	{
		var invalidBoardId = Guid.Empty;

		var command = new GetBoardColumnsQuery(invalidBoardId, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var invalidBoardId = Guid.NewGuid();

		var command = new GetBoardColumnsQuery(invalidBoardId, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveRoleOnBoard()
	{
		var anotherUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(anotherUser);

		var command = new GetBoardColumnsQuery(_board.Id, false);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldReturnAllBoardColumnsIncludeArchived()
	{
		var queryResult = await SendAsync(new GetBoardColumnsQuery(_board.Id, true));

		queryResult.BoardColumns.Should().HaveCount(_board.BoardColumns.Count);

		var firstReturnedColumn = queryResult.BoardColumns.First();
		firstReturnedColumn.Title.Should().Be(_boardColumn1.Title);
		firstReturnedColumn.IsArchived.Should().Be(_boardColumn1.IsArchived);
		firstReturnedColumn.Items.Should().HaveCount(_boardColumn1.Items.Count);
		firstReturnedColumn.Items.First().Title.Should().Be(_card1.Title);
		firstReturnedColumn.Items.Skip(1).First().Title.Should().Be(_card2.Title);

		var secondReturnedColumn = queryResult.BoardColumns.Skip(1).First();
		secondReturnedColumn.Title.Should().Be(_boardColumn2.Title);
		secondReturnedColumn.IsArchived.Should().Be(_boardColumn2.IsArchived);
		secondReturnedColumn.Items.Should().HaveCount(_boardColumn2.Items.Count);
		secondReturnedColumn.Items.First().Title.Should().Be(_card3.Title);
		secondReturnedColumn.Items.Skip(1).First().Title.Should().Be(_card4.Title);
	}

	[Test]
	public async Task ShouldReturnOnlyNotArchivedBoardColumns()
	{
		var queryResult = await SendAsync(new GetBoardColumnsQuery(_board.Id, false));

		queryResult.BoardColumns.Should().HaveCount(1);

		var returnedColumn = queryResult.BoardColumns.First();
		returnedColumn.Title.Should().Be(_boardColumn1.Title);
		returnedColumn.IsArchived.Should().BeFalse();
		returnedColumn.Items.Should().HaveCount(_boardColumn1.Items.Count);
		returnedColumn.Items.First().Title.Should().Be(_card1.Title);
		returnedColumn.Items.Skip(1).First().Title.Should().Be(_card2.Title);
	}
}