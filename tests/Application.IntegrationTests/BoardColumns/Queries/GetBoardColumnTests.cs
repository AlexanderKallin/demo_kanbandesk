using FluentAssertions;
using KanbanDesk.Application.BoardColumns.Queries.GetBoardColumn;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardColumns.Queries;

using static Testing;

public class GetBoardColumnTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_card1 = new BoardColumnItem { Title = "Item #1" };
		_card2 = new BoardColumnItem { Title = "Item #2" };
		_boardColumn = new BoardColumn { Title = "Column #1", Items = new List<BoardColumnItem> { _card1, _card2 } };
		_board = new Board { Title = "Board #1", BoardColumns = new List<BoardColumn> { _boardColumn } };
		await CreateBoard(_board);
	}

	private BoardColumnItem _card1 = null!;
	private BoardColumnItem _card2 = null!;
	private BoardColumn _boardColumn = null!;
	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimalFields()
	{
		var invalidBoardId = Guid.Empty;

		var command = new GetBoardColumnQuery(invalidBoardId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireColumnToExists()
	{
		var invalidBoardColumnId = Guid.NewGuid();

		var command = new GetBoardColumnQuery(invalidBoardColumnId);

		await FluentActions.Invoking(() => SendAsync(command)).Should().ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveRoleOnBoard()
	{
		var anotherUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(anotherUser);

		var command = new GetBoardColumnQuery(_boardColumn.Id);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldReturnBoardColumn()
	{
		var returnedColumn = await SendAsync(new GetBoardColumnQuery(_boardColumn.Id));

		returnedColumn.Title.Should().Be(_boardColumn.Title);
		returnedColumn.IsArchived.Should().Be(_boardColumn.IsArchived);
		returnedColumn.Items.Should().HaveCount(_boardColumn.Items.Count(item => item.IsArchived == false));
	}
}