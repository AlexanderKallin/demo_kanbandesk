using FluentAssertions;
using KanbanDesk.Application.BoardEventLogMessages.Queries;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.BoardEventLogMessages.Queries;

using static Testing;

public class GetBoardEventLogMessagesTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board { Title = "Board #1" };
		await CreateBoard(_board);
	}

	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidBoardId = Guid.Empty;

		var command = new GetBoardEventLogMessagesQuery(invalidBoardId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var invalidBoardId = Guid.NewGuid();

		var command = new GetBoardEventLogMessagesQuery(invalidBoardId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveRole()
	{
		var anotherUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(anotherUser);

		var command = new GetBoardEventLogMessagesQuery(_board.Id);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldReturnBoardEventLogMessages()
	{
		var message1 = new BoardEventLogMessage
		{
			BoardId = _board.Id,
			InitiatorUserId = DefaultUser.Id,
			DateTime = DateTime.Now,
			Message = "Test message #1",
			SubjectId = Guid.NewGuid()
		};
		await AddAsync(message1);

		var message2 = new BoardEventLogMessage
		{
			BoardId = _board.Id,
			InitiatorUserId = DefaultUser.Id,
			DateTime = DateTime.Now,
			Message = "Test message #2",
			SubjectId = Guid.NewGuid()
		};
		await AddAsync(message2);

		var eventLogMessages = await SendAsync(new GetBoardEventLogMessagesQuery(_board.Id));

		eventLogMessages.Messages.Count.Should().Be(2);

		var firstReturnedMessage = eventLogMessages.Messages.First();
		firstReturnedMessage.BoardId.Should().Be(message1.BoardId);
		firstReturnedMessage.InitiatorUserId.Should().Be(message1.InitiatorUserId);
		firstReturnedMessage.DateTime.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		firstReturnedMessage.Message.Should().Be(message1.Message);
		firstReturnedMessage.SubjectId.Should().Be(message1.SubjectId);

		var secondReturnedMessage = eventLogMessages.Messages.Skip(1).First();
		secondReturnedMessage.BoardId.Should().Be(message2.BoardId);
		secondReturnedMessage.InitiatorUserId.Should().Be(message2.InitiatorUserId);
		secondReturnedMessage.DateTime.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		secondReturnedMessage.Message.Should().Be(message2.Message);
		secondReturnedMessage.SubjectId.Should().Be(message2.SubjectId);
	}
}