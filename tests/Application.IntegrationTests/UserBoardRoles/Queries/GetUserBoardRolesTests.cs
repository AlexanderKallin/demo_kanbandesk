using FluentAssertions;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRoles;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.UserBoardRoles.Queries;

using static Testing;

public class GetUserBoardRolesTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board
		{
			Title = "Board #1"
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidBoardId = Guid.Empty;

		var command = new GetUserBoardRolesQuery(invalidBoardId);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var nonExistentBoardId = Guid.NewGuid();

		var command = new GetUserBoardRolesQuery(nonExistentBoardId);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveRole()
	{
		var anotherUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(anotherUser);

		var command = new GetUserBoardRolesQuery(_board.Id);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldReturnUserBoardRoles()
	{
		var anotherUser = await CreateAndRunAsUserWithRole(_board, BoardRole.Worker);

		var queryResult = await SendAsync(new GetUserBoardRolesQuery(_board.Id));

		queryResult.UserBoardRoles.Count.Should().Be(2);

		var firstUserBoardRole = queryResult.UserBoardRoles.First();
		firstUserBoardRole.UserId.Should().Be(DefaultUser.Id);
		firstUserBoardRole.BoardId.Should().Be(_board.Id);
		firstUserBoardRole.BoardRole.Should().Be(BoardRole.Admin);

		var secondUserBoardRole = queryResult.UserBoardRoles.Skip(1).First();
		secondUserBoardRole.UserId.Should().Be(anotherUser.Id);
		secondUserBoardRole.BoardId.Should().Be(_board.Id);
		secondUserBoardRole.BoardRole.Should().Be(BoardRole.Worker);
	}
}