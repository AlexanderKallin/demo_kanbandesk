using FluentAssertions;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRole;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.UserBoardRoles.Queries;

using static Testing;

public class GetUserBoardRoleTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board
		{
			Title = "Board #1"
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidUserId = string.Empty;
		var invalidBoardId = Guid.Empty;

		var command = new GetUserBoardRoleQuery(invalidUserId, invalidBoardId);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var nonExistentBoardId = Guid.NewGuid();

		var command = new GetUserBoardRoleQuery(DefaultUser.Id, nonExistentBoardId);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveRole()
	{
		var anotherUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(anotherUser);

		var command = new GetUserBoardRoleQuery(DefaultUser.Id, _board.Id);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldRequireTargetUserToHaveRole()
	{
		var targetUser = await CreateIdentityUserAsync();

		var command = new GetUserBoardRoleQuery(targetUser.Id, _board.Id);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldReturnUserBoardRole()
	{
		var userBoardRole = await SendAsync(new GetUserBoardRoleQuery(DefaultUser.Id, _board.Id));

		userBoardRole.UserId.Should().Be(DefaultUser.Id);
		userBoardRole.BoardId.Should().Be(_board.Id);
		userBoardRole.BoardRole.Should().Be(BoardRole.Admin);
	}
}