using FluentAssertions;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.UserBoardRoles.Commands.DeleteUserBoardRole;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.UserBoardRoles.Commands;

using static Testing;

public class DeleteUserBoardRoleTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board
		{
			Title = "Board #1"
		};
		await CreateBoard(_board);

		_targetUser = await CreateIdentityUserAsync();
		await AssignBoardRoleAsync(_targetUser, _board, BoardRole.Worker);
	}

	private Board _board = null!;
	private ApplicationUser _targetUser = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidUserId = string.Empty;
		var invalidBoardId = Guid.Empty;

		var command = new DeleteUserBoardRoleCommand(invalidUserId, invalidBoardId);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var nonExistentBoardId = Guid.NewGuid();

		var command = new DeleteUserBoardRoleCommand(_targetUser.Id, nonExistentBoardId);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new DeleteUserBoardRoleCommand(_targetUser.Id, _board.Id);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldRequireTargetUserToHaveRole()
	{
		var anotherUser = await CreateIdentityUserAsync();

		var command = new DeleteUserBoardRoleCommand(anotherUser.Id, _board.Id);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldNotBePossibleToDeleteBoardCreatorRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Admin);

		var command = new DeleteUserBoardRoleCommand(DefaultUser.Id, _board.Id);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldDeleteUserBoardRole()
	{
		await SendAsync(new DeleteUserBoardRoleCommand(_targetUser.Id, _board.Id));

		var userBoardRole = await FindAsync<UserBoardRole>(_targetUser.Id, _board.Id);

		userBoardRole.Should().BeNull();
	}
}