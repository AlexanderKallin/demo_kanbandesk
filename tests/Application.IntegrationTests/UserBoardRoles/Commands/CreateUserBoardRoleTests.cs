using FluentAssertions;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.UserBoardRoles.Commands.CreateUserBoardRole;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.UserBoardRoles.Commands;

using static Testing;

public class CreateUserBoardRoleTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board
		{
			Title = "Board #1"
		};
		await CreateBoard(_board);

		_targetUser = await CreateIdentityUserAsync();
	}

	private Board _board = null!;
	private ApplicationUser _targetUser = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidUserId = string.Empty;
		var invalidBoardId = Guid.Empty;

		var command = new CreateUserBoardRoleCommand(invalidUserId, invalidBoardId, BoardRole.Admin);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var nonExistentBoardId = Guid.NewGuid();

		var command = new CreateUserBoardRoleCommand(_targetUser.Id, nonExistentBoardId, BoardRole.Worker);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new CreateUserBoardRoleCommand(_targetUser.Id, _board.Id, BoardRole.Admin);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldRequireTargetUserToExists()
	{
		const string nonExistentUserId = "invalid";
		var command = new CreateUserBoardRoleCommand(nonExistentUserId, _board.Id, BoardRole.Admin);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireTargetUserToNotHaveRole()
	{
		await AssignBoardRoleAsync(_targetUser, _board, BoardRole.Admin);

		var command = new CreateUserBoardRoleCommand(_targetUser.Id, _board.Id, BoardRole.Worker);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldCreateUserBoardRole()
	{
		await SendAsync(new CreateUserBoardRoleCommand(_targetUser.Id, _board.Id, BoardRole.Admin));

		var userBoardRole = await FindAsync<UserBoardRole>(_targetUser.Id, _board.Id);

		userBoardRole.Should().NotBeNull();
		userBoardRole!.BoardRole = BoardRole.Admin;
	}
}