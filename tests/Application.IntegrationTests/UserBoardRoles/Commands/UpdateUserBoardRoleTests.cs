using FluentAssertions;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Application.UserBoardRoles.Commands.UpdateUserBoardRole;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.UserBoardRoles.Commands;

using static Testing;

public class UpdateUserBoardRoleTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board
		{
			Title = "Board #1"
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidUserId = string.Empty;
		var invalidBoardId = Guid.Empty;

		var command = new UpdateUserBoardRoleCommand(invalidUserId, invalidBoardId, BoardRole.Admin);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var targetUser = await CreateIdentityUserAsync();
		var nonExistentBoardId = Guid.NewGuid();

		var command = new UpdateUserBoardRoleCommand(targetUser.Id, nonExistentBoardId, BoardRole.Admin);

		await FluentActions.Invoking(() =>
				SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminRole()
	{
		var targetUser = await CreateIdentityUserAsync();

		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new UpdateUserBoardRoleCommand(targetUser.Id, _board.Id, BoardRole.Admin);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldNotBeAbleToChangeHisOwnRole()
	{
		var command = new UpdateUserBoardRoleCommand(DefaultUser.Id, _board.Id, BoardRole.Guest);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldRequireTargetUserToHaveRole()
	{
		var targetUser = await CreateIdentityUserAsync();

		var command = new UpdateUserBoardRoleCommand(targetUser.Id, _board.Id, BoardRole.Guest);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldNotBePossibleToChangeBoardCreatorRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Admin);

		var command = new UpdateUserBoardRoleCommand(DefaultUser.Id, _board.Id, BoardRole.Guest);
		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldNotBeAbleToSetTheSameRole()
	{
		var targetUser = await CreateIdentityUserAsync();
		await AssignBoardRoleAsync(targetUser, _board, BoardRole.Admin);

		var command = new UpdateUserBoardRoleCommand(targetUser.Id, _board.Id, BoardRole.Admin);

		await FluentActions
			.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<BadRequestException>();
	}

	[Test]
	public async Task ShouldUpdateUserBoardRole()
	{
		var targetUser = await CreateIdentityUserAsync();
		await AssignBoardRoleAsync(targetUser, _board, BoardRole.Guest);

		await SendAsync(new UpdateUserBoardRoleCommand(targetUser.Id, _board.Id, BoardRole.Admin));

		var targetUserBoardRole = await FindAsync<UserBoardRole>(targetUser.Id, _board.Id);

		targetUserBoardRole.Should().NotBeNull();
		targetUserBoardRole!.BoardRole.Should().Be(BoardRole.Admin);
	}
}