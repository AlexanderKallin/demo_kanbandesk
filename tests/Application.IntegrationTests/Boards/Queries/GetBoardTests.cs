using FluentAssertions;
using KanbanDesk.Application.Boards.Queries.GetBoard;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.Boards.Queries;

using static Testing;

public class GetBoardTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board
		{
			Title = "Board #1",
			BoardColumns = new List<BoardColumn>
			{
				new()
				{
					Title = "Column #1",
					IsArchived = true,
					Items = new List<BoardColumnItem> { new() { Title = "Item #1" } }
				},
				new()
				{
					Title = "Column #2",
					Items = new List<BoardColumnItem>
					{
						new() { Title = "Item #1" }, new() { Title = "Item #2", IsArchived = true }
					}
				}
			}
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidBoardId = Guid.Empty;

		var command = new GetBoardQuery(invalidBoardId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var invalidBoardId = Guid.NewGuid();

		var command = new GetBoardQuery(invalidBoardId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldRequireRoleInBoard()
	{
		var anotherUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(anotherUser);

		var command = new GetBoardQuery(_board.Id);
		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldReturnBoard()
	{
		var returnedBoard = await SendAsync(new GetBoardQuery(_board.Id));

		returnedBoard.Title.Should().Be(_board.Title);
		returnedBoard.BoardColumns.Should().HaveCount(_board.BoardColumns.Count(column => column.IsArchived == false));
		returnedBoard
			.BoardColumns.First()
			.Items.Should()
			.HaveCount(_board.BoardColumns.First().Items.Count(item => item.IsArchived == false));
	}
}