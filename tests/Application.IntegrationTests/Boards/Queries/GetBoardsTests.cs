using FluentAssertions;
using KanbanDesk.Application.Boards.Queries.GetBoards;
using KanbanDesk.Domain.Entities;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.Boards.Queries;

using static Testing;

public class GetBoardsTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_boards.Add(new Board
		{
			Title = "Board #1",
			BoardColumns = new List<BoardColumn>
			{
				new()
				{
					Title = "Column #1",
					Items = new List<BoardColumnItem>
					{
						new() { Title = "Item #1", Description = "Item #1 description" }
					}
				}
			}
		});

		_boards.Add(new Board
		{
			Title = "Board #2",
			BoardColumns = new List<BoardColumn>
			{
				new() { Title = "Column #1", IsArchived = true },
				new()
				{
					Title = "Column #2",
					Items = new List<BoardColumnItem>
					{
						new() { Title = "BoardColumnItem #1", Description = "Item #1 description" },
						new() { Title = "BoardColumnItem #2" }
					}
				},
				new()
				{
					Title = "Column #3",
					Items = new List<BoardColumnItem>
					{
						new() { Title = "BoardColumnItem #1", IsArchived = true },
						new() { Title = "BoardColumnItem #2" },
						new() { Title = "BoardColumnItem #3", IsArchived = true }
					}
				}
			}
		});

		foreach (var board in _boards)
			await CreateBoard(board);
	}

	private readonly List<Board> _boards = new();

	[Test]
	public async Task ShouldNotReturnBoardsWhereUserDontHaveRole()
	{
		var anotherUser = await CreateIdentityUserAsync();
		await RunAsUserAsync(anotherUser);

		var queryResult = await SendAsync(new GetBoardsQuery());

		queryResult.Boards.Should().HaveCount(0);
	}

	[Test]
	public async Task ShouldReturnAllAvailableBoards()
	{
		var queryResult = await SendAsync(new GetBoardsQuery());

		queryResult.Boards.Should().HaveCount(_boards.Count);
	}

	[Test]
	public async Task ShouldReturnAllNotArchivedColumnsForBoards()
	{
		var queryResult = await SendAsync(new GetBoardsQuery());

		var nonArchivedColumns = _boards
			.SelectMany(board => board.BoardColumns)
			.Where(column => column.IsArchived == false)
			.ToList();

		var receivedNonArchivedColumns = queryResult.Boards
			.SelectMany(board => board.BoardColumns)
			.Where(column => column.IsArchived == false)
			.ToList();

		receivedNonArchivedColumns.Count.Should().Be(nonArchivedColumns.Count);
		receivedNonArchivedColumns.Select(column => column.IsArchived).Should().AllBeEquivalentTo(false);

		receivedNonArchivedColumns.Select(column => column.Id)
			.Should()
			.BeEquivalentTo(nonArchivedColumns.Select(column => column.Id));

		receivedNonArchivedColumns.Select(column => column.BoardId)
			.Should()
			.BeEquivalentTo(nonArchivedColumns.Select(column => column.BoardId));

		receivedNonArchivedColumns.Select(column => column.Title)
			.Should()
			.BeEquivalentTo(nonArchivedColumns.Select(column => column.Title));
	}

	[Test]
	public async Task ShouldReturnAllNotArchivedItemsForBoardColumns()
	{
		var queryResult = await SendAsync(new GetBoardsQuery());

		var nonArchivedItems = _boards
			.SelectMany(board => board.BoardColumns)
			.Where(column => column.IsArchived == false)
			.SelectMany(column => column.Items)
			.Where(item => item.IsArchived == false)
			.ToList();

		var receivedNonArchivedItems = queryResult.Boards
			.SelectMany(board => board.BoardColumns)
			.Where(column => column.IsArchived == false)
			.SelectMany(column => column.Items)
			.Where(item => item.IsArchived == false)
			.ToList();

		receivedNonArchivedItems.Count.Should().Be(nonArchivedItems.Count);
		receivedNonArchivedItems.Select(item => item.IsArchived).Should().AllBeEquivalentTo(false);

		receivedNonArchivedItems.Select(item => item.Id)
			.Should()
			.BeEquivalentTo(nonArchivedItems.Select(item => item.Id));

		receivedNonArchivedItems.Select(item => item.BoardColumnId)
			.Should()
			.BeEquivalentTo(nonArchivedItems.Select(item => item.BoardColumnId));

		receivedNonArchivedItems.Select(item => item.Title)
			.Should()
			.BeEquivalentTo(nonArchivedItems.Select(item => item.Title));

		receivedNonArchivedItems.Select(item => item.Description)
			.Should()
			.BeEquivalentTo(nonArchivedItems.Select(item => item.Description));
	}
}