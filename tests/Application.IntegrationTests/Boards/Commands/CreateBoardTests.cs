using FluentAssertions;
using KanbanDesk.Application.Boards.Commands.CreateBoard;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.Boards.Commands;

using static Testing;

public class CreateBoardTests : KanbanDeskTestFixture
{
	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidTitle = string.Empty;

		var command = new CreateBoardCommand(invalidTitle);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldCreateBoard()
	{
		var command = new CreateBoardCommand("Board #1");
		var boardId = await SendAsync(command);

		var board = await FindAsync<Board>(boardId);

		board.Should().NotBeNull();
		board!.Title.Should().Be(command.Title);
		board.Created.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		board.CreatedBy.Should().Be(DefaultUser.Id);
		board.LastModifiedBy.Should().Be(DefaultUser.Id);
		board.LastModified.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
	}

	[Test]
	public async Task ShouldCreateAdminUserBoardRoleForInitiatorUser()
	{
		var boardId = await SendAsync(new CreateBoardCommand("Board #1"));

		var userBoardRole = await FindAsync<UserBoardRole>(DefaultUser.Id, boardId);

		userBoardRole.Should().NotBeNull();
		userBoardRole!.BoardRole.Should().Be(BoardRole.Admin);
	}
}