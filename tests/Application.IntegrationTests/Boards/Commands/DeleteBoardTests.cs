using FluentAssertions;
using KanbanDesk.Application.Boards.Commands.DeleteBoard;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.Boards.Commands;

using static Testing;

public class DeleteBoardTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board
		{
			Title = "Board #1"
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidBoardId = Guid.Empty;

		var command = new DeleteBoardCommand(invalidBoardId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var invalidBoardId = Guid.NewGuid();

		var command = new DeleteBoardCommand(invalidBoardId);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToBeBoardCreator()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Admin);

		await FluentActions.Invoking(() => SendAsync(new DeleteBoardCommand(_board.Id)))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldDeleteBoard()
	{
		await SendAsync(new DeleteBoardCommand(_board.Id));

		var deletedBoard = await FindAsync<Board>(_board.Id);

		deletedBoard.Should().BeNull();
	}
}