using FluentAssertions;
using KanbanDesk.Application.Boards.Commands.UpdateBoard;
using KanbanDesk.Application.Common.Exceptions;
using KanbanDesk.Domain.Entities;
using KanbanDesk.Domain.Enums;
using NUnit.Framework;

namespace KanbanDesk.Application.IntegrationTests.Boards.Commands;

using static Testing;

public class UpdateBoardTests : KanbanDeskTestFixture
{
	[SetUp]
	public async Task Setup()
	{
		_board = new Board
		{
			Title = "Board #1"
		};
		await CreateBoard(_board);
	}

	private Board _board = null!;

	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var invalidBoardId = Guid.Empty;
		var invalidTitle = string.Empty;

		var command = new UpdateBoardCommand(invalidBoardId, invalidTitle);

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldRequireBoardToExists()
	{
		var invalidBoardId = Guid.NewGuid();

		var command = new UpdateBoardCommand(invalidBoardId, "Updated board title");

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<NotFoundException>();
	}

	[Test]
	public async Task ShouldRequireInitiatorUserToHaveAdminRole()
	{
		await CreateAndRunAsUserWithRole(_board, BoardRole.Guest);

		var command = new UpdateBoardCommand(_board.Id, "Updated board title");

		await FluentActions.Invoking(() => SendAsync(command))
			.Should()
			.ThrowAsync<ForbiddenAccessException>();
	}

	[Test]
	public async Task ShouldUpdateBoard()
	{
		var anotherUser = await CreateAndRunAsUserWithRole(_board, BoardRole.Admin);

		var command = new UpdateBoardCommand(_board.Id, "Updated board title");
		await SendAsync(command);

		var board = await FindAsync<Board>(_board.Id);

		board.Should().NotBeNull();
		board!.Title.Should().Be(command.Title);
		board.Created.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		board.CreatedBy.Should().Be(DefaultUser.Id);
		board.LastModified.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
		board.LastModifiedBy.Should().Be(anotherUser.Id);
	}
}