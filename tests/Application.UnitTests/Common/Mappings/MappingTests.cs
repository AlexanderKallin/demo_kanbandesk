﻿using System.Runtime.Serialization;
using AutoMapper;
using KanbanDesk.Application.BoardColumnItems.Queries.GetBoardColumnItem;
using KanbanDesk.Application.BoardColumns.Queries.GetBoardColumn;
using KanbanDesk.Application.BoardEventLogMessages.Queries;
using KanbanDesk.Application.Boards.DTOs;
using KanbanDesk.Application.Boards.Queries.GetBoard;
using KanbanDesk.Application.Boards.Queries.GetBoards;
using KanbanDesk.Application.Common.Mappings;
using KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRole;
using KanbanDesk.Application.UserBoardRoles.Queries.GetUserBoardRoles;
using KanbanDesk.Domain.Entities;
using NUnit.Framework;

namespace KanbanDesk.Application.UnitTests.Common.Mappings;

public class MappingTests
{
	private readonly IConfigurationProvider _configuration;
	private readonly IMapper _mapper;

	public MappingTests()
	{
		_configuration = new MapperConfiguration(config =>
			config.AddProfile<MappingProfile>());

		_mapper = _configuration.CreateMapper();
	}

	[Test]
	public void ShouldHaveValidConfiguration()
	{
		_configuration.AssertConfigurationIsValid();
	}

	[Test]
	// Boards feature
	[TestCase(typeof(Board), typeof(BoardVm))]
	[TestCase(typeof(Board), typeof(BoardDto))]
	[TestCase(typeof(BoardColumn), typeof(BoardColumnDto))]
	[TestCase(typeof(BoardColumnItem), typeof(BoardColumnItemDto))]
	// BoardColumns feature
	[TestCase(typeof(BoardColumn), typeof(BoardColumnVm))]
	[TestCase(typeof(BoardColumnItem), typeof(BoardColumns.DTOs.BoardColumnItemDto))]
	[TestCase(typeof(BoardColumn), typeof(BoardColumns.Queries.GetBoardColumns.BoardColumnDto))]
	// Items feature
	[TestCase(typeof(BoardColumnItem), typeof(BoardColumnItemVm))]
	[TestCase(typeof(BoardColumnItem), typeof(BoardColumnItemDto))]
	//UserBoardRoles feature
	[TestCase(typeof(UserBoardRole), typeof(UserBoardRoleVm))]
	[TestCase(typeof(UserBoardRole), typeof(UserBoardRoleDto))]
	//BoardEventLogMessages feature
	[TestCase(typeof(BoardEventLogMessage), typeof(BoardEventLogMessageDto))]
	public void ShouldSupportMappingFromSourceToDestination(Type source, Type destination)
	{
		var instance = GetInstanceOf(source);

		_mapper.Map(instance, source, destination);
	}

	private object GetInstanceOf(Type type)
	{
		if (type.GetConstructor(Type.EmptyTypes) != null)
			return Activator.CreateInstance(type)!;

		// Type without parameterless constructor
		return FormatterServices.GetUninitializedObject(type);
	}
}