﻿using KanbanDesk.Application.Boards.Commands.CreateBoard;
using KanbanDesk.Application.Common.Behaviours;
using KanbanDesk.Application.Common.Interfaces;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace KanbanDesk.Application.UnitTests.Common.Behaviours;

public class RequestLoggerTests
{
	private Mock<ICurrentUserService> _currentUserService = null!;
	private Mock<IIdentityService> _identityService = null!;
	private Mock<ILogger<CreateBoardCommand>> _logger = null!;

	[SetUp]
	public void Setup()
	{
		_logger = new Mock<ILogger<CreateBoardCommand>>();
		_currentUserService = new Mock<ICurrentUserService>();
		_identityService = new Mock<IIdentityService>();
	}

	[Test]
	public async Task ShouldCallGetUserNameAsyncOnceIfAuthenticated()
	{
		_currentUserService.Setup(x => x.UserId).Returns(Guid.NewGuid().ToString());

		LoggingBehaviour<CreateBoardCommand> requestLogger =
			new(_logger.Object, _currentUserService.Object,
				_identityService.Object);

		await requestLogger.Process(new CreateBoardCommand("Board #1"), new CancellationToken());

		_identityService.Verify(i => i.GetUserNameAsync(It.IsAny<string>()), Times.Once);
	}

	[Test]
	public async Task ShouldNotCallGetUserNameAsyncOnceIfUnauthenticated()
	{
		LoggingBehaviour<CreateBoardCommand> requestLogger =
			new(_logger.Object, _currentUserService.Object,
				_identityService.Object);

		await requestLogger.Process(new CreateBoardCommand("Board #1"), new CancellationToken());

		_identityService.Verify(i => i.GetUserNameAsync(It.IsAny<string>()), Times.Never);
	}
}