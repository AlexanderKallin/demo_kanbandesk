# DEMO_KanbanDesk
Demo project - backend api of kanban desk made with CleanArchitecture (DDD) approach.

Features:
* Basic functionality (CRUD + Soft-delete)
* Authentication with JWT
* Individual user roles for each board (Admin, Worker, Guest)
* Board actions log
* Unit and integration testing
* Swagger support
* Docker-compose support

## How to run
```bash
docker compose up --build
```

## Ports
* `80` - backend
* `1433` - mssql

## Technologies
* .NET 7
* ASP.NET Core 7
* EntityFramework Core 7
* ASP.NET Core Identity
* MediatR
* AutoMapper
* NSwag
* NUnit 3
* FluentAssertions
* Moq
* Respawn
* SqlServer 2019
* Docker-compose
